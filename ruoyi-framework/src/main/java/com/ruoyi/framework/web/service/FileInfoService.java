package com.ruoyi.framework.web.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.domain.server.SysFile;
import com.ruoyi.system.domain.CmsMenu;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.mapper.CmsNavMapper;
import com.ruoyi.system.mapper.SysFileInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
@Service("fileInfos")
public class FileInfoService {


    @Autowired
    SysFileInfoMapper sysFileInfoMapper;

    public List<String> fileList(String ids){
        if(StringUtils.isNull(ids)){
            return  null;
        }
        return sysFileInfoMapper.selectList(new QueryWrapper<SysFileInfo>().in("id", ids.split(";"))).stream().map(SysFileInfo::getFilePath).collect(Collectors.toList());
    }
}
