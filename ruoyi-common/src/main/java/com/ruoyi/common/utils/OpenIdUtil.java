package com.ruoyi.common.utils;


import com.ruoyi.common.pay.wepay.WeAppletConfig;
import com.ruoyi.common.pay.wepay.WePayConfig;
import com.ruoyi.common.utils.http.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * openid工具类
 * @author Penglei
 * @date  2020年6月4日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */

@Component
public class OpenIdUtil {

    @Autowired
    private WePayConfig wePayConfig;

    /**
    * 方法描述
    * @author Penglei
    * @date 获取用户openid
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param code
    * @return  Object
    */
    public Object getOpenId(String code) {
        String param = "appid=" + WeAppletConfig.appId + "&secret=" + WeAppletConfig.appSecret + "&js_code=" + code + "&grant_type=authorization_code";
        return HttpUtils.sendGet(this.wePayConfig.getOpenIdUrl(), param);
    }
}
