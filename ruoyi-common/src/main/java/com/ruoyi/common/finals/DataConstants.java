package com.ruoyi.common.finals;

/**
 * 服务层常量
 */
public class DataConstants {

    public static final String OTHER_DATA_UNIQUE = "0";
    public static final String OTHER_DATA_NOT_UNIQUE = "1";
    public static final String BATCH_UPDATE_SUCCESS = "1";
    public static final String BATCH_UPDATE_ERROR = "0";
    public static final String NAGETIVE = "-1";
    public static final String QUERY_DATA_SUCCESS = "获取成功";
    public static final String QUERY_DATA_ISNULL = "获取失败,没有数据";
    public static final String INSERT_DATA_SUCCESS = "数据添加成功";
    public static final String INSERT_DATA_ISNULL = "数据添加失败";
    public static final String UPDATE_SUCCESS = "更新成功";
    public static final String UPDATE_FIELD = "更新失败";
    public static final String DATA_IS_EXSITS = "存在相同数据";
    public static final String COMMENT_SUCCESS = "评论成功";
    public static final String COMMENT_FIELD = "评论失败，你已经评论过";
    public static final String REGISTER_SUCCESS = "注册成功";
    public static final String REGISTER_FIELD = "注册失败";
    public static final String SMS_SEND_SUCCESS = "短信发送成功";
    public static final String SMS_SEND_FIALD = "短信发送失败";
    public static final String SMS_CODE = "OK";
    public static final String VERIF_FILED = "验证码不正确";
    public static final String USER_EXIST = "用户已存在或请更换用户名";
    public static final String USER_NOT_EXIST = "用户已存在或请更换用户名";
    public static final String NOT_USER = "没有找到用户";
    public static final String NOT_ID = "缺少参数用户ID-uid";
    public static final String NOT_USERNAME = "缺少参数用户名";
    public static final String USER_NOT_MATCH = "用户不匹配";
    public static final String ORDER_NO_NULL = "订单号为空或不存在";
    public static final String PAY_SUCCESS = "支付成功";
    public static final String PHONE_CHANGE_SUCCESS = "手机号更换成功";
    public static final String PHONE_CHANGE_FILED = "手机号更换失败";
    public static final String PHONE_REPEAT = "手机号不能与原手机号一致";
    public static final String REPLAY_SUCCESS = "接取者内容回复成功";
    public static final String REPLAY_FAIL = "接取者内容回复失败或您已经回复过";
    public static final String SEND_DATA_SUCCESS = "消息监听发送成功";
    public static final String SEND_DATA_FAIL = "消息监听发送失败";
    public static final String CREATE_ORDER_SUCCESS = "订单创建成功";
    public static final String CREATE_ORDER_FAIL = "订单创建失败";
    public static final String CREG_SUCCESS = "报名成功";
    public static final String CREG_FAIL = "报名失败";
    public static final String UPLOAD_FIAL = "上传失败";
    public static final String LOGIN_SUCCESS = "登录成功";
    public static final String LOGIN_FIAL = "登录失败";
    public static final String RK = "insertReverse";
    public static final String cReg = "cReg";
    public static final String CREATEORDER = "createOrder";
    public static final String courseComment = "courseComment";
    public static final String FAVORITE_SUCCESS = "收藏成功";
    public static final String FAVORITE_FAIL = "收藏失败";
    public static final String PAY_FAIL = "支付失败";
    public static final String OK = "成功";
    public static final String FIAL = "失败";
    public static final String SESSION_ERROR = "获取预支付交易会话标识失败";
    public static final String REFUND_SUCCESS = "退款/提现成功";
    public static final String REFUND_FAIL = "退款/提现失败";
    public static final String PERCENTAGE = "%";
    public static final String PARENT_ID = "parentId";
    public static final String SUB_ID = "subId";
    public static final String TITLE = "title";
    public static final String UID = "u_id";
    public static final String PREFIX_CLASS = "ac.";

    public static final String SAVE_SUCCESS = "保存成功";
    public static final String SAVE_FIAL = "保存失败";

    public static final String SUCCESS = "SUCCESS";
    public static final Object RESULT_CODE = "result_code";
    public static final String CHARSET = "UTF-8";
    public static final String OKS = "OK";
    public static final Float YEAR =  0.03F;
    public static final Float QUARTER = 0.02F;
    public static final Float MONTHLY =0.01F;
    public static final String DONT_KNOW_ERR = "未知错误";
    public static final String REMOVE_SUCCESS = "删除成功";
    public static final String REMOVE_FAIL = "删除失败";
}
