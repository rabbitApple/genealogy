package com.ruoyi.common.pay;


import cn.hutool.core.collection.CollectionUtil;
import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayUtil;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.common.pay.wepay.WePayConfig;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * @author MisPeng
 */
@SuppressWarnings("ALL")
@Primary
@Service
@Slf4j
public class OrderTransferService {


    private static final String TRANSFERS_PAY = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
    private static final String TRANSFERS_PAY_QUERY = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo";
    @Autowired
    private WePayConfig wePayConfig;


    /**
     * 提现功能
     * @param openid
     * @param user
     * @param token
     * @param amount
     * @return
     * @throws Exception
     */
    public AjaxResult transferPay(String openid, String user, String token, BigDecimal amount) throws Exception {
        log.info("[/transfer/pay]");
        WXPay wxPay = new WXPay(this.wePayConfig);
        Map restmap = new HashMap();

        HashMap  transferMap = new HashMap();
        try {
            transferMap.put("mch_appid", this.wePayConfig.getTxID());
            transferMap.put("mchid", this.wePayConfig.getMchID());
            transferMap.put("nonce_str", WXPayUtil.generateNonceStr());
            transferMap.put("partner_trade_no", SnowflakeIdWorker.getUUID());
            transferMap.put("openid", openid);
            transferMap.put("check_name", "NO_CHECK");
            transferMap.put("re_user_name", user);
            transferMap.put("amount",String.valueOf(new BigDecimal(amount.doubleValue()).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).intValue()));
            transferMap.put("desc", "零钱提现");
            transferMap.put("spbill_create_ip", "127.0.0.1");
            transferMap.put("sign", WXPayUtil.generateSignature(transferMap, this.wePayConfig.getKey()));
            String restxml = wxPay.requestWithCert("https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers", transferMap, 3000, 3000);
            restmap = WXPayUtil.xmlToMap(restxml);
        } catch (Exception var10) {
            log.error(var10.getMessage(), var10);
        }

        if (CollectionUtil.isNotEmpty(restmap) && "SUCCESS".equals(restmap.get("result_code"))) {
            log.info("转账成功：" + (String)restmap.get("err_code") + ":" + (String)restmap.get("err_code_des"));
            transferMap = new HashMap();
            transferMap.put("partner_trade_no", restmap.get("partner_trade_no"));
            transferMap.put("payment_no", restmap.get("payment_no"));
            transferMap.put("payment_time", restmap.get("payment_time"));
            return AjaxResult.success(DataConstants.REFUND_SUCCESS);
        } else if (CollectionUtil.isNotEmpty(restmap)) {
            log.info("转账失败：" + (String)restmap.get("err_code") + ":" + (String)restmap.get("err_code_des"));
        }

        return AjaxResult.error(DataConstants.REFUND_FAIL + (String)restmap.get("err_code_des"));
    }

    /**
    * 支付宝体现操作
    * @author Penglei
    * @date 2020年4月29日
    * @version 1.0.0
    * @updateVersion 1.0.0
    */
//    public String aliTrasfer(Float money,String order_no,String account) throws AlipayApiException {
//        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.url, AlipayConfig.app_id, AlipayConfig.private_key, AlipayConfig.format, AlipayConfig.charset, AlipayConfig.public_key, AlipayConfig.signtype);
////      构建体现对象
//        AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
//        JSONObject jsonObject = new JSONObject();
//        JSONObject j1 = new JSONObject();
//        jsonObject.put("out_biz_no",order_no);
//        jsonObject.put("amount",money);
//        jsonObject.put("payee_type","ALIPAY_LOGONID");
//        jsonObject.put("payee_account",account);
//        jsonObject.put("biz_scene","DIRECT_TRANSFER");
//        jsonObject.put("payer_show_name","维修夜市");
//        jsonObject.put("remark","提现");
//        request.setBizContent(jsonObject.toJSONString());
//
//        AlipayFundTransToaccountTransferResponse response = alipayClient.execute(request);
//        if(response.isSuccess()){
//            log.info("code:{}",response.getCode());
//            log.info("msg:{}",response.getMsg());
//            log.info("sub_msg:{}",response.getSubMsg());
//            log.info("支付宝调用成功.....");
//            return "success";
//        } else {
//            log.info("code:{}",response.getCode());
//            log.info("msg:{}",response.getMsg());
//            log.info("sub_msg:{}",response.getSubMsg());
//            log.info("支付宝调用失败.....");
//            return "fail";
//        }
//    }
















}
