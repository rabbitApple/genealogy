package com.ruoyi.common.pay.wepay;

import com.ruoyi.common.config.Global;

public class WeAppletConfig {

    public static String appId = Global.getConfig("wxApplet.appId");
    public static String appSecret = Global.getConfig("wxApplet.appSecret");
    public static String matchID = Global.getConfig("wxApplet.matchID");
}
