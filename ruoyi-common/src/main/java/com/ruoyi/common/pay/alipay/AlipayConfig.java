package com.ruoyi.common.pay.alipay;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Description   :  支付宝支付配置类
 * @Author        :  MP
 * @CreateDate    :  2020/2/14 17:02
 * @UpdateUser    :  MP
 * @UpdateDate    :  2020/2/14 17:02
 * @UpdateRemark  :  修改内容
 * @Version       :  1.0
 */
@SuppressWarnings("ALL")
@Setter
@Getter
@ToString
public class AlipayConfig {

    /**
     * APPID
     */
    public static String app_id = "";
    public static String public_key = "";
    public static String private_key = "";
    /**
     * 异步回调接口:得放到服务器上，且使用域名解析 IP
     */
    public static String notify_url = "https://加密域名/api/order/notify";
    /**
     * 支付宝网关（注意沙箱alipaydev，正式则为 alipay）不需要修改
     */
    public static String url = "https://openapi.alipay.com/gateway.do";
//    public static String url = "https://openapi.alipaydev.com/gateway.do";
    public static String transferUrl = "alipay.fund.trans.uni.transfer";
    /**
     * 编码类型
     */
    public static String charset = "UTF-8";

    /**
     * 数据类型
     */
    public static String format = "json";

    /**
     * 签名类型
     */
    public static String signtype = "RSA2";

    /**
     * 开发者应用公钥证书路径
     */
    public  static String certPath = "";

    /**
     * 支付宝公钥证书文件路径
     */
    public static String alipayPublicCertPath = "";
    /**
     * 支付宝CA根证书文件路径
     */
    public static String rootCertPath = "";
}
