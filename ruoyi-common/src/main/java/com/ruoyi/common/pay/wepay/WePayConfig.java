package com.ruoyi.common.pay.wepay;


import com.github.wxpay.sdk.WXPayConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @Description   :  微信支付配置模型
 * @Author        :  MP
 * @CreateDate    :  2020/2/14 12:33
 * @UpdateUser    :  MP
 * @UpdateDate    :  2020/2/14 12:33
 * @UpdateRemark  :  修改内容
 * @Version       :  1.0
 */
@Component
@ConfigurationProperties(prefix = "wxpay")
@PropertySource(value = {"classpath:common.yml"})
public class WePayConfig implements WXPayConfig {

    /**
     * appID
     */
    private String appID;

    /**
     * 提现ID
     */
    private String txID;

    /**
     * 商户号
     */
    private String mchID;

    /**
     * API 密钥
     */
    private String key;

    /**
     * API证书绝对路径 (本项目放在了 resources/cert/wxpay/apiclient_cert.p12")
     */
    private String certPath;

    /**
     * HTTP(S) 连接超时时间，单位毫秒
     */
    private int httpConnectTimeoutMs = 8000;

    /**
     * HTTP(S) 读数据超时时间，单位毫秒
     */
    private int httpReadTimeoutMs = 10000;

    /**
     * 微信支付异步通知地址
     */
    private String payNotifyUrl;

    /**
     * 微信退款异步通知地址
     */
    private String refundNotifyUrl;

    /**
     * openid地址
     */
    private String openIdUrl;

    /**
     * 获取商户证书内容（这里证书需要到微信商户平台进行下载）
     *
     * @return 商户证书内容
     */
    @Override
    public InputStream getCertStream() {
        InputStream certStream  = null;
        try {
            certStream = new FileInputStream(certPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return certStream;
    }

    public String getOpenIdUrl() {
        return this.openIdUrl;
    }

    @Value("${openIdUrl}")
    public void setOpenIdUrl(String openIdUrl) {
        this.openIdUrl = openIdUrl;
    }

    public String getTxID() {
        return txID;
    }
    @Value("${txID}")
    public void setTxID(String txID) {
        this.txID = txID;
    }

    public String getAppID() {
        return appID;
    }
    @Value("${appID}")
    public void setAppID(String appID) {
        this.appID = appID;
    }


    public String getMchID() {
        return mchID;
    }
    @Value("${mchID}")
    public void setMchID(String mchID) {
        this.mchID = mchID;
    }


    public String getKey() {
        return key;
    }
    @Value("${key}")
    public void setKey(String key) {
        this.key = key;
    }


    public String getCertPath() {
        return certPath;
    }
    @Value("${certPath}")
    public void setCertPath(String certPath) {
        this.certPath = certPath;
    }

    public int getHttpConnectTimeoutMs() {
        return httpConnectTimeoutMs;
    }

    public void setHttpConnectTimeoutMs(int httpConnectTimeoutMs) {
        this.httpConnectTimeoutMs = httpConnectTimeoutMs;
    }

    public int getHttpReadTimeoutMs() {
        return httpReadTimeoutMs;
    }

    public void setHttpReadTimeoutMs(int httpReadTimeoutMs) {
        this.httpReadTimeoutMs = httpReadTimeoutMs;
    }


    public String getPayNotifyUrl() {
        return payNotifyUrl;
    }
    @Value("${payNotifyUrl}")
    public void setPayNotifyUrl(String payNotifyUrl) {
        this.payNotifyUrl = payNotifyUrl;
    }

    public String getRefundNotifyUrl() {
        return refundNotifyUrl;
    }

    public void setRefundNotifyUrl(String refundNotifyUrl) {
        this.refundNotifyUrl = refundNotifyUrl;
    }
}
