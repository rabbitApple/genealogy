package com.ruoyi.common.pay.wepay.domain;

import lombok.Data;

/**
 * @Description   :  统一下单模型
 * @Author        :  MP
 * @CreateDate    :  2020/2/14 12:12
 * @UpdateUser    :  MP
 * @UpdateDate    :  2020/2/14 12:12
 * @UpdateRemark  :  修改内容
 * @Version       :  1.0
 */
@Data
public class UnifiedorderResult {
    private String return_code;
    private String return_msg;
    private String appid;
    private String mch_id;
    private String device_info;
    private String nonce_str;
    private String sign;
    private String result_code;
    private String err_code;
    private String err_code_des;
    private String trade_type;
    private String prepay_id;
}
