package com.ruoyi.common.base.service;


import java.util.List;

/**
* 基础服务类
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
public interface IBaseService<T>{


    /**
     * 数据列表
     * @param t
     * @return int
     */
    public List<T> dataList(T t);


    /**
     * 保存数据
     * @param t
     * @return int
     */
    public int insert(T t);


    /**
     * 修改数据
     * @param t
     * @return int
     */
    public int edit(T t);


    /**
     * 删除数据
     * @param t
     * @return int
     */
    public int delete(Integer t);


    /**
     * 查询数据
     * @param id
     * @return
     */
    public  T getDataById(Integer id);


    /**
     * 查询是否唯一
     * @param t
     * @return
     */
    public  String checkNameUnique(String t,Integer id);


    /**
     * 实体查询信息
     * @param t
     * @return
     */
    public T queryData(T t);

}
