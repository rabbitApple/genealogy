package com.ruoyi.common.base.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.base.service.IBaseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* 基础服务实现
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
@SuppressWarnings("ALL")
public class IBaseServiceImpl<T> implements IBaseService<T> {


    /**
     * 注入
     */
    @Autowired
    private BaseMapper<T> baseMapper;

    /**
     * 数据列表,默认不筛选
     * @param t
     * @return int
     */
    @Override
    public List<T> dataList(T t) {
        return baseMapper.selectList(new QueryWrapper<T>());
    }

    /**
     * 保存数据
     * @param t
     * @return int
     */
    @Override
    @Transactional(rollbackFor = Exception.class,isolation = Isolation.READ_COMMITTED)
    public int insert(T t) {
        return baseMapper.insert(t);
    }

    /**
     * 修改数据
     * @param t
     * @return int
     */
    @Override
    @Transactional(rollbackFor = Exception.class,isolation = Isolation.READ_COMMITTED)
    public int edit(T t) {
        return baseMapper.updateById(t);
    }

    /**
     * 删除数据
     * @param t
     * @return int
     */
    @Override
    @Transactional(rollbackFor = Exception.class,isolation = Isolation.READ_COMMITTED)
    public int delete(Integer t) {
        return baseMapper.deleteById(t);
    }


    /**
     * 根据ID查询数据
     * @param id
     * @return
     */
    @Override
    public  T getDataById(Integer id){
        return  baseMapper.selectById(id);
    }





    /**
     * 查询是否唯一
     * @param t
     * @return
     */
    @Override
    public String checkNameUnique(String t,Integer id) {
        return "";
    }

    /**
     * 实体查询信息
     *
     * @param t
     * @return
     */
    @Override
    public T queryData(T t) {
        return baseMapper.selectOne(new QueryWrapper<T>());
    }
}
