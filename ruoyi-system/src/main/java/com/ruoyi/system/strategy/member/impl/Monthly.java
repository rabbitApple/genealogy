package com.ruoyi.system.strategy.member.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.CmsUser;
import com.ruoyi.system.mapper.CmsUserMapper;
import com.ruoyi.system.strategy.member.MemberStrategy;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * 包月
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年6月9日
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Slf4j
public class Monthly implements MemberStrategy {
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private Calendar calendar = null;


    @Override
    public AjaxResult appendTime(Integer uid, CmsUserMapper cmsUserMapper) throws ParseException {
        log.info("包月");
        Optional<CmsUser> cmsUser = Optional.ofNullable(cmsUserMapper.selectById(uid));
        if (!cmsUser.isPresent()) {
            return AjaxResult.error(DataConstants.DONT_KNOW_ERR);
        }
        CmsUser user = cmsUser.get();
        String time = user.getDeathTime();
//            获取当前日期
        calendar = Calendar.getInstance();
        if (StringUtils.isEmpty(time)) {
//          当前月份基础上加一个月
            calendar.add(Calendar.MONTH, 1);
        } else {
            Date mtime = format.parse(time);
            calendar.setTime(mtime);
            calendar.add(Calendar.MONTH, 1);
        }
        user.setDeathTime(format.format(new Date(calendar.getTimeInMillis())));
        int ret = cmsUserMapper.updateById(user);
        return ret > 0
                ? AjaxResult.success("购买会员成功")
                : AjaxResult.error("购买会员失败");
    }
}
