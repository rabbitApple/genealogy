package com.ruoyi.system.strategy.member;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.mapper.CmsUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.ParseException;

/**
 * 策略接收器,专门接收策略实现的算法
 * @author Penglei
 * @date 2020年6月9日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Slf4j
public class MemberContext {
    private MemberStrategy memberStrategy;

    public MemberContext(MemberStrategy strategy) {
        this.memberStrategy = strategy;
    }
    /**
    * 时间追加
    * @author Penglei
    * @date 2020年6月9日
    * @version 1.0.0
    * @updateVersion 1.0.0
    */
    public AjaxResult appendTime(Integer uid, CmsUserMapper cmsUserMapper) throws ParseException {
        log.info("用户ID："+uid);
        return  memberStrategy.appendTime(uid, cmsUserMapper);
    }
}
