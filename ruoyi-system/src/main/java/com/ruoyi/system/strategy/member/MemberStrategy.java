package com.ruoyi.system.strategy.member;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.mapper.CmsUserMapper;

import java.text.ParseException;

/**
 * 会员购买追加策略
 * @author Penglei
 * @date 2020年6月9日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface MemberStrategy {

    /**
    * 时间追加
    * @author Penglei
    * @date 2020年6月9日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param uid
    * @param cmsUserMapper
    */
    AjaxResult appendTime(Integer uid, CmsUserMapper cmsUserMapper) throws ParseException;
}
