package com.ruoyi.system.domain;


import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.ApiBaseEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@TableName("cms_user")
@Getter
@Setter
@ToString
public class CmsUser extends ApiBaseEntity {

    /**
     * ID
     */
    private Integer id;
    /**
     * 用户手机号码
     */
    private String userPhone;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 头像
     */
    private String avatarUrl;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 用户类型
     */
    private Integer userType;
    /**
     * 状态
     */
    private Integer status;
    /**
     * openid
     */
    private String openId;
    /**
     * 城市
     */
    private String city;
    /**
     * 籍贯
     */
    private String province;
    /**
     * 国家
     */
    private String country;

    /**
     * 会员过期时间
     */
    private String deathTime;
}
