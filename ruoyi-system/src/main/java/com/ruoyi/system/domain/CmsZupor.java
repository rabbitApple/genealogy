package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.ApiBaseEntity;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.DTO.CmsZuporDTO;
import com.ruoyi.system.domain.VO.CmsZuporVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 族谱模型
 * @author Penglei
 * @date  2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@TableName("cms_zupor")
@Getter
@Setter
@ToString
@ApiModel(value = "族谱模型")
public class CmsZupor extends ApiBaseEntity {

    /**
     * id
     */
    private Integer id;

    /**
     * 族谱名
     */
    @ApiModelProperty(value = "族谱名称",required = true)
    private String zname;

    /**
     * 族谱姓氏
     */
    @ApiModelProperty(value = "族谱姓氏",required = true)
    private String sname;

    /**
     * 始祖名
     */
    @ApiModelProperty(value = "始祖名",required = true)
    private String aname;

    /**
     * 起始代数
     */
    @ApiModelProperty(value = "起始代数",required = true)
    private String startDs;

    /**
     * 祖籍地址
     */
    @ApiModelProperty(value = "祖籍地址",required = true)
    private String homeAddress;

    /**
     * 卷号
     */
    @ApiModelProperty(value = "卷号",required = true)
    private String vNumber;

    /**
     * 序言
     */
    @ApiModelProperty(value = "序言",required = false)
    private String xuyan;

    /**
     * 姓氏来源
     */
    @ApiModelProperty(value = "姓氏来源",required = false)
    private String xingshi;

    /**
     * 家训
     */
    @ApiModelProperty(value = "家训",required = false)
    private String jiaxun;

    /**
     * 封面ID
     */
    @ApiModelProperty(value = "封面ID",required = true)
    private Integer fileId;

    /**
     * 欧式族谱图组
     */
    private String oushiZupor;

    /**
     * 苏式族谱图组
     */
    private String sushiZupor;

    /**
     * 世系图图组
     */
    private String treeZupor;

    /**
     * 城市
     */
    private String city;

    /**
     * 用户ID
     */
    private Integer uid;

    @TableField(exist = false)
    private String filePath;
}
