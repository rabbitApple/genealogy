package com.ruoyi.system.domain.VO;

import com.ruoyi.system.domain.CmsInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 信息响应模型
 * @author Penglei
 * @date 2020年5月26日14:14:51
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@Getter
@Setter
@ToString
public class CmsInfoVO extends CmsInfo {
}
