package com.ruoyi.system.domain.VO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * 族人树
 * @author Penglei
 * @date  2020年6月12日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@Getter
@Setter
@ToString
public class PeopleTreeVO {
    private Integer id;

    /**
     * 名称
     */
    private String title;

    /**
     * 配偶名称
     */
    private String poName;

    /**
     * 是否存在子嗣
     */
    private Integer child;

    private String spouseId;

    /**
     * 子嗣列表
     */
    private List<PeopleTreeVO> children;
}
