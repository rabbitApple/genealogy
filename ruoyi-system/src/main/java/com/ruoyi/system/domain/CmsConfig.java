package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
* 内容管理配置模型
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
@TableName("cms_config")
@Getter
@Setter
@ToString
public class CmsConfig  implements Serializable {

    /**
     * ID
     */
    private Integer id;

    /**
     * 名称
     */
    private String appName;

    /**
     * 欢迎字符
     */
    private String welcomeText;

    /**
     * 隐私政策
     */
    private String privacy;


    /**
     * 协议
     */
    private String proxy;

    /**
     * 广告1
     */
    private String ad1;

    /**
     * 广告2
     */
    private String ad2;

    /**
     * 广告3
     */
    private String ad3;

    /**
     * 族谱定制内容
     */
    private String makeContent;

}
