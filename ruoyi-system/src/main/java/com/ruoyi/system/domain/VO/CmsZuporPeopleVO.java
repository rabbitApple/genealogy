package com.ruoyi.system.domain.VO;

import com.ruoyi.system.domain.CmsZuporPeople;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
/**
 * 类描述
 * @author Penglei
 * @date  2020年6月12日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@Getter
@Setter
@ToString
public class CmsZuporPeopleVO extends CmsZuporPeople {

    private Integer child;
    /**
     * 儿子列表
     */
    private List<CmsZuporPeopleVO> childrens;

    /**
     * 配偶
     */
    private CmsZuporPeople peiou;

    /**
     * 头像
     */
    private String headUrl;
}
