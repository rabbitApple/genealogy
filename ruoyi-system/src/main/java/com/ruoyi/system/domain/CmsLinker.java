package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


/**
 * 友链模型
 * @author Penglei
 * @date 2020年5月28日10:43:42
 * @version 1.0.0
 */
@TableName("cms_linker")
@Getter
@Setter
@ToString
public class CmsLinker implements Serializable {

    /**
     * ID
     */
    private Integer id;

    /**
     * 友链
     */
    private String name;

    /**
     * 链接地址
     */
    private String linker;
}
