package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 会员类型模型
 * @author Penglei
 * @date 2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@TableName("cms_member")
@Getter
@Setter
@ToString
public class CmsMemberType implements Serializable {

    /**
     * id
     */
    private Integer id;

    /**
     * 会员说明
     */
    private String title;

    /**
     * 会员价格
     */
    private Float price;
}
