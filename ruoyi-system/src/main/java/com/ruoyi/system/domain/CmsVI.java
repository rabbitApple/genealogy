package com.ruoyi.system.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CmsVI{
    private Integer id;

    private Integer infoId;

    private Integer uid;
}
