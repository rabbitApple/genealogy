package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


/**
 * 城市表
 * @author Penglei
 * @date 2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@TableName("s_provinces")
@Getter
@Setter
@ToString
public class City implements Serializable {

    private Integer id;

    private String cityName;

    private Integer parentId;

    private String shortName;

    private String cityCode;

    private String mergerName;
}
