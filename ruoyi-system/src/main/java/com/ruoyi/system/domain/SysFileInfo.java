package com.ruoyi.system.domain;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 文件模型
 * @author Penglei
 * @date  2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@TableName("sys_file_info")
@Getter
@Setter
@ToString
public class SysFileInfo implements Serializable {

    /**
     * 文件ID
     */
    private Integer id;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 文件所属模块
     */
    private String keyss;

    /**
     * 用户ID，-1代表系统
     */
    private Integer uid;


    /**
     * 所属信息ID
     */
    private Integer infoId;


    /**
     * 批量查询ID
     */
    @TableField(exist = false)
    private String ids;

}
