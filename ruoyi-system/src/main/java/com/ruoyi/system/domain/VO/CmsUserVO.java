package com.ruoyi.system.domain.VO;

import com.ruoyi.system.domain.CmsUser;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CmsUserVO extends CmsUser {
}
