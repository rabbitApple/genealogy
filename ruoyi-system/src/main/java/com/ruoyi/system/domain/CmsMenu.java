package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


/**
* 导航模型
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
@TableName("cms_nav")
@Getter
@Setter
@ToString
public class CmsMenu implements Serializable {

    /**
     * 栏目ID
     */
    private Integer id;

    /**
     * 栏目名称
     */
    private String name;

    /**
     * 栏目页图片
     */
    private String image;

    /**
     * seo信息
     */
    private String seo;

    /**
     * 父栏目
     */
    private String pid;

    /**
     * 导航图标
     */
    private String  icon;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 单页栏目内容
     */
    private String content;

    /**
     * 栏目类型
     */
    private Integer type;

    /**
     *
     */
    private String intro;

    //-------------------------------------------------------------------------------------------------------

    /**
     * 标记不存入数据库
     */
    @TableField(exist = false)
    private String parentName;

}
