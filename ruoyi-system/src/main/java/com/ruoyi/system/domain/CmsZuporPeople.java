package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.ApiBaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 族人模型
 * @author Penglei
 * @date  2020年6月8日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@TableName("cms_zupor_people")
@ApiModel(value = "族人模型")
@Getter
@Setter
@ToString
public class CmsZuporPeople extends ApiBaseEntity {

    /**
     * ID
     */
    private Integer id;
    /**
     * 姓
     */
    private String surname;
    /**
     * 名
     */
    private String name;
    /**
     * 曾用名
     */
    private String oldName;
    /**
     * 表字
     */
    private String tword;
    /**
     * 别号
     */
    private String nickName;
    /**
     * 世代
     */
    private String generation;
    /**
     * 排行
     */
    private String sorts;

    /**
     * 性别
     */
    private String gender;

    /**
     * 是否已故
     */
    private Integer exits;

    /**
     * 联系电话
     */
    private String phone;
    /**
     * 学历
     */
    private String education;
    /**
     * 职业
     */
    private String occupation;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 出身时辰
     */
    private String timeOfBirth;
    /**
     * 逝世日期
     */
    private String dateOfDeath;

    /**
     * 逝世时辰
     */
    private String timeOfDeath;
    /**
     * 安葬地址
     */
    private String burialAddress;
    /**
     * 配偶ID
     */
    private Integer spouseId;

    /**
     * 头像ID
     */
    private Integer fileId;

    /**
     * 族人分类
     */
    private Integer level;

    /**
     * 族谱ID
     */
    private Integer zid;

    /**
     * 上级ID
     */
    private Integer pid;

    /**
     * 出生地
     */
    private String address;
}
