package com.ruoyi.system.domain.VO;

import com.ruoyi.system.domain.CmsZupor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CmsZuporVO extends CmsZupor {

    private String filePath;
}
