package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.ApiBaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;


/**
 * 订单模型
 * @author Penglei
 * @date  2020年6月9日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@Getter
@Setter
@ToString
@TableName("cms_order")
public class CmsOrder extends ApiBaseEntity {

    private Integer id;

    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 购买的会员等级ID
     */
    private Integer mid;
    /**
     * 会员ID
     */
    private Integer uid;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 类型
     */
    private Integer code;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 支付方式，默认微信支付
     */
    private Integer payType;
    /**
     * 数量
     */
    private Integer totalNumber;
    /**
     * 单价
     */
    private BigDecimal singlePrice;
}
