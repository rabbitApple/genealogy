package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 信息模型
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年5月26日
 */
@TableName("cms_news")
@Getter
@Setter
@ToString
public class CmsInfo extends BaseEntity {

    /**
     * 信息ID
     */
    private Integer id;


    /**
     * 信息标题
     */
    private String title;

    /**
     * 信息内容
     */
    private String content;

    /**
     * 缩略图
     */
    private String image;

    /**
     * 简短标题
     */
    private String intro;

    /**
     * 浏览量
     */
    private Integer views;
}
