package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsMemberType;

/**
 * 会员类型
 * @author Penglei
 * @date  2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface CmsMemberMapper extends BaseMapper<CmsMemberType> {

    /**
    * 批量删除
    * @author Penglei
    * @date 2020年6月5日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param ids
    * @return int
    */
    int deleteByIds(String[] ids);
}
