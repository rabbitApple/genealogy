package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsOrder;

/**
 * 不想描述了，但我又想描述，订单Mapper
 * @author Penglei
 * @date  2020年6月9日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface CmsOrderMapper extends BaseMapper<CmsOrder> {
}
