package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.City;

/**
 * 城市
 * @author Penglei
 * @date  2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface CityMapper extends BaseMapper<City> {
}
