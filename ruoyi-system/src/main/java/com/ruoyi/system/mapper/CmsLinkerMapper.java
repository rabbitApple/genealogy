package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsLinker;

/**
 * 友链Mapper
 * @author Penglei
 * @date 2020年5月28日10:49:27
 * @version 1.0.0
 */
public interface CmsLinkerMapper extends BaseMapper<CmsLinker> {

    /**
    * 批量删除
    * @author Penglei
    * @date 2020年5月28日11:01:32
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param ids
    * @return int
    */
    int deleteByIds(String[] ids);
}
