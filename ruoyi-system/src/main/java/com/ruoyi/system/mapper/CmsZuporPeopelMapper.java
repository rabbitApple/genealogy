package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruoyi.system.domain.CmsZuporPeople;
import com.ruoyi.system.domain.VO.CmsZuporPeopleVO;
import com.ruoyi.system.domain.VO.PeopleTreeVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 族人Mapper
 * @author Penglei
 * @date 2020年6月8日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface CmsZuporPeopelMapper extends BaseMapper<CmsZuporPeople> {


    /**
     * 条件查询
     * @author Penglei
     * @date 2020年6月12日
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param  wrapper
     * @return List
     */
    @Select("SELECT\n" +
            "\tzup.*,fi.file_path as head_url,\n" +
            "\tCOUNT( zup1.pid ) AS child \n" +
            "FROM\n" +
            "\t`cms_zupor_people` AS zup\n" +
            "\tLEFT JOIN cms_zupor_people AS zup1 ON zup1.pid = zup.id \n" +
            "\tLEFT JOIN sys_file_info AS fi ON fi.id = zup.file_id \n" +
            "${ew.customSqlSegment};")
    List<CmsZuporPeopleVO> selectListOfChildSize(@Param(Constants.WRAPPER) Wrapper wrapper);


    /**
     * 条件查询
     * @author Penglei
     * @date 2020年6月12日
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param  wrapper
     * @return List
     */
    @Select("SELECT\n" +
            "\tzup.id,zup.old_name as title,zup.spouse_id,\n" +
            "\tCOUNT( zup1.pid ) AS child \n" +
            "FROM\n" +
            "\t`cms_zupor_people` AS zup\n" +
            "\tLEFT JOIN cms_zupor_people AS zup1 ON zup1.pid = zup.id \n" +
            "${ew.customSqlSegment};")
    List<PeopleTreeVO> selectListOfChildTree(@Param(Constants.WRAPPER) Wrapper wrapper);

    /**
    * 条件删除
    * @author Penglei
    * @date 2020年6月12日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param  wrapper
    * @return int
    */
    @Delete("DELETE FROM cms_zupor_people\n" +
            "${ew.customSqlSegment}")
    int deleteByMapWrapper(@Param(Constants.WRAPPER) Wrapper wrapper);
}
