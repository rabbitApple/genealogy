package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsMenu;
import org.apache.ibatis.annotations.Param;

/**
* 栏目底层
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
public interface CmsNavMapper extends BaseMapper<CmsMenu> {


    /**
    * 查询栏目
    * @author Penglei
    * @date 2020年5月26日
     * @param id
    * @version 1.0.0
    * @updateVersion 1.0.0
     * @return CmsMenu
    */
    CmsMenu selectMenuById(@Param("id") Integer id);
}
