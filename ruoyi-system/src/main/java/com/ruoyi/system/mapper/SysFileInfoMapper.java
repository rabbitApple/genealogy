package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.SysFileInfo;

/**
 * 文件mapper
 * @author Penglei
 * @date  2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface SysFileInfoMapper extends BaseMapper<SysFileInfo> {

    /**
    * 批量删除
    * @author Penglei
    * @date 2020年6月3日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param toStrArray
    * @return int
    */
    int deleteByIds(String[] toStrArray);
}
