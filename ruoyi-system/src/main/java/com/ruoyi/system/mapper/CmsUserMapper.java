package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsUser;

public interface CmsUserMapper extends BaseMapper<CmsUser> {
}
