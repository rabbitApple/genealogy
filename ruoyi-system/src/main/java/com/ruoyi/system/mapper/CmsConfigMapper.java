package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsConfig;

/**
* 内容管理系统配置Mapper
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
public interface CmsConfigMapper extends BaseMapper<CmsConfig> {

    /**
    * 查询配置
    * @author Penglei
    * @date 2020年5月25日
     * @param id
     * @return CmsConfig
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    CmsConfig selectConfig(Integer id);


    /**
    * 更新配置
    * @author Penglei
    * @date 2020年5月25日
     * @param cmsConfig
     * @return int
    * @version 1.0.0
    * @updateVersion 1.0.0
    */
    int updateConfig(CmsConfig cmsConfig);

}
