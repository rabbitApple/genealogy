package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsInfo;
import com.ruoyi.system.domain.VO.CmsInfoVO;

import java.util.List;

/**
* 信息Mapper
* @author Penglei
* @date 2020年5月26日
* @version 1.0.0
*/
public interface CmsInfoMapper extends BaseMapper<CmsInfo> {

    /**
    * 信息列表
    * @author Penglei
    * @date 2020年5月26日14:07:34
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param cmsInfo
    * @return  List
    */
    List<CmsInfoVO> selectInfoList(CmsInfo cmsInfo);

    /**
    * 批量删除信息
    * @author Penglei
    * @date 2020年5月26日16:47:43
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param ids
    * @return int
    */
    int deleteByIds(String[] ids);
}
