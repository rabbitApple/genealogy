package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.CmsVI;

public interface CmsVIMapper extends BaseMapper<CmsVI> {
}
