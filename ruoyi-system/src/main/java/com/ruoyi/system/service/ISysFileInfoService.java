package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.SysFileInfo;

import java.util.List;

/**
 * 文件信息服务接口
 * @author Penglei
 * @date  2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface ISysFileInfoService  extends IBaseService<SysFileInfo> {


    /**
    * 获取族谱封面列表
    * @author Penglei
    * @date 2020年6月3日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param sysFileInfo
    * @return  List<SysFileInfo>
    */
    public List<SysFileInfo> selectFaceList(SysFileInfo sysFileInfo);


    /**
    * 获取指定文件
    * @author Penglei
    * @date 2020年6月5日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param sysFileInfo
    * @return List
    */
    public List<SysFileInfo> listByType(SysFileInfo sysFileInfo);

    /**
    * 批量删除
    * @author Penglei
    * @date 2020年6月3日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param ids
    * @return int
    */
    public int deleteByIds(String ids);



}
