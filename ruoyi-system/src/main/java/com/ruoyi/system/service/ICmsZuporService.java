package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.CmsZupor;
import com.ruoyi.system.domain.VO.CmsZuporVO;

import java.util.List;

/**
 * 族谱操作服务接口
 * @author Penglei
 * @date 2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface ICmsZuporService extends IBaseService<CmsZupor> {


    /**
     * 删除信息
     * @author Penglei
     * @date 2020年5月26日15:52:39
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param ids
     * @return  int
     */
    int deleteByIds(String ids);


    /**
    * 族谱列表，族谱案例列表
    * @author Penglei
    * @date 2020年6月5日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param page
    * @return List
    */
    List<CmsZupor> dataList(Integer page,String createBy);


    /**
    * 获取族谱详情
    * @author Penglei
    * @date 2020年6月5日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param id
    * @return CmsZupor
    */
    CmsZuporVO getZuporDetail(Integer id);

}
