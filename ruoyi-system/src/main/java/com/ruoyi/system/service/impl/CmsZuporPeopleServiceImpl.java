package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.CmsZuporPeople;
import com.ruoyi.system.domain.VO.CmsZuporPeopleVO;
import com.ruoyi.system.domain.VO.PeopleTreeVO;
import com.ruoyi.system.mapper.CmsZuporPeopelMapper;
import com.ruoyi.system.service.ICmsZuporPeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 族人接口实现
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年6月8日
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class CmsZuporPeopleServiceImpl extends IBaseServiceImpl<CmsZuporPeople>
        implements ICmsZuporPeopleService {

    @Autowired
    CmsZuporPeopelMapper cmsZuporPeopelMapper;

    /**
     * 删除数据
     *
     * @param t
     * @return int
     */
    @Override
    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    public int delete(Integer t) {
        int count = cmsZuporPeopelMapper.selectCount(new QueryWrapper<CmsZuporPeople>()
                .eq("pid", t));
        if (count > 0) {
            return 0;
        }
        cmsZuporPeopelMapper.deleteByMapWrapper(new QueryWrapper<CmsZuporPeople>()
                .eq("spouse_id", t));
        int del = cmsZuporPeopelMapper.deleteById(t);
        return del;
    }

    /**
     * 添加族人，配偶
     *
     * @param cmsZuporPeople
     * @return int
     */
    @Override
    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    public int insert(CmsZuporPeople cmsZuporPeople) {
        int ret = super.insert(cmsZuporPeople);
        if (!StringUtils.isNull(cmsZuporPeople.getSpouseId())
                && ret > 0) {
            CmsZuporPeople po = cmsZuporPeopelMapper.selectById(cmsZuporPeople.getSpouseId());
            po.setSpouseId(cmsZuporPeople.getId());
            cmsZuporPeopelMapper.updateById(po);
        }
        return cmsZuporPeople.getId();
    }

    /**
     * 获取族人树列表
     *
     * @param id
     * @return List<CmsZuporPeople>
     * @author Penglei
     * @date 2020年6月9日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public Optional<CmsZuporPeopleVO> dataTreeById(Integer id) {
        Optional<CmsZuporPeople> c = Optional.ofNullable(cmsZuporPeopelMapper.selectOne(new QueryWrapper<CmsZuporPeople>()
                .eq("generation", 1)
                .eq("zid", id)
                .eq("deleted", 0)));
        List<CmsZuporPeopleVO> list = null;
        CmsZuporPeopleVO cs = ReflectUtil.newInstance(CmsZuporPeopleVO.class);
        if (c.isPresent()) {
            BeanUtil.copyProperties(c.get(), cs);
            list = tree(id, c.get().getId());
            cs.setChildrens(tree(id, c.get().getId()));
            cs.setChild(list.size());
            cs.setPeiou(c.get().getSpouseId() == null ? null : cmsZuporPeopelMapper.selectById(c.get().getSpouseId()));
        } else {
            return Optional.empty();
        }
        return Optional.ofNullable(cs);
    }

    /**
     * 获取族人树
     *
     * @param id
     * @return List<CmsZuporPeople>
     * @author Penglei
     * @date 2020年6月9日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<PeopleTreeVO> dataTree(Integer id) {
        Optional<CmsZuporPeople> c = Optional.ofNullable(cmsZuporPeopelMapper.selectOne(new QueryWrapper<CmsZuporPeople>()
                .eq("generation", 1)
                .eq("zid", id)
                .eq("deleted", 0)));
        List<PeopleTreeVO> list = new ArrayList<>();
        PeopleTreeVO cs = ReflectUtil.newInstance(PeopleTreeVO.class);
        if (c.isPresent()) {
            cs.setId(c.get().getId());
            cs.setTitle(c.get().getOldName());
            cs.setChildren(trees(id, c.get().getId()));
            list.add(cs);
        } else {
            return null;
        }
        return list;
    }

    /**
     * 后端树结构
     *
     * @param id
     * @param pid
     * @return
     */
    private List<PeopleTreeVO> trees(Integer id, Integer pid) {
        List<PeopleTreeVO> lr = cmsZuporPeopelMapper
                .selectListOfChildTree(new QueryWrapper<PeopleTreeVO>()
                        .eq("zup.zid", id)
                        .eq("zup.pid", pid)
                        .eq("zup.deleted", 0)
                        .orderByAsc("zup.pid,zup.generation")
                        .groupBy("zup.id"));
        lr.forEach(item -> {
            if (item.getChild() > 0) {
                item.setChildren(trees(id, item.getId()));
            }
            if (!StringUtils.isNull(item.getSpouseId()) && !DataConstants.NAGETIVE
                    .equals(item.getSpouseId())) {
                item.setPoName(cmsZuporPeopelMapper.selectById(item.getSpouseId()).getOldName());
            }
        });
        return lr;
    }

    /**
     * 前端树结构
     *
     * @param id
     * @param pid
     * @return
     */
    private List<CmsZuporPeopleVO> tree(Integer id, Integer pid) {
        List<CmsZuporPeopleVO> lr = cmsZuporPeopelMapper
                .selectListOfChildSize(new QueryWrapper<CmsZuporPeople>()
                        .eq("zup.zid", id)
                        .eq("zup.pid", pid)
                        .eq("zup.deleted", 0)
                        .orderByAsc("zup.pid,zup.generation")
                        .groupBy("zup.id"));
        lr.forEach(item -> {
            if (item.getChild() > 0) {
                item.setChildrens(tree(id, item.getId()));
            }
            if (!StringUtils.isNull(item.getSpouseId())) {
                item.setPeiou(cmsZuporPeopelMapper.selectById(item.getSpouseId()));
            }
        });
        return lr;
    }

    /**
     * 获取族人当前有几代人
     *
     * @param id
     * @return CmsZuporPeople
     * @author Penglei
     * @date 2020年6月9日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public CmsZuporPeople getPeopleLevel(Integer id) {
        List<CmsZuporPeople> c = cmsZuporPeopelMapper.selectList(new QueryWrapper<CmsZuporPeople>()
                .eq("zid", id)
                .eq("deleted", 0)
                .orderByDesc("generation"));
        return c.size() > 0 ?  c.get(0) : null;
    }
}
