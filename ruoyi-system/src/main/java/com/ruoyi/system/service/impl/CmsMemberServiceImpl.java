package com.ruoyi.system.service.impl;

import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.CmsMemberType;
import com.ruoyi.system.mapper.CmsMemberMapper;
import com.ruoyi.system.service.ICmsMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * 服务实现
 * @author Penglei
 * @date
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class CmsMemberServiceImpl extends IBaseServiceImpl<CmsMemberType>
implements ICmsMemberService {

    /**
     * 会员说明
     */
    @Autowired
    private CmsMemberMapper cmsMemberMapper;
    /**
     * 删除信息
     * @param ids
     * @return int
     * @author Penglei
     * @date 2020年5月26日15:52:39
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int deleteByIds(String ids) {
        return cmsMemberMapper.deleteByIds(Convert.toStrArray(ids));
    }
}
