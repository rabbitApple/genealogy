package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.City;
import com.ruoyi.system.mapper.CityMapper;
import com.ruoyi.system.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 城市服务接口实现
 * @author Penglei
 * @date  2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class CityServiceImpl extends IBaseServiceImpl<City> implements ICityService {


    @Autowired
    private CityMapper cityMapper;

    /**
     * 数据列表,默认不筛选
     *
     * @param city
     * @return int
     */
    @Override
    public List<City> dataList(Integer city) {
        QueryWrapper<City> querys = new QueryWrapper<City>();
        if (StringUtils.isNull(city)){
            querys.eq("parent_id","10000");
        }else {
            querys.eq("parent_id",city);
        }
        return cityMapper.selectList(querys);
    }
}
