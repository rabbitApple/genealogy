package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.pay.wepay.WePayConfig;
import com.ruoyi.system.domain.CmsUser;
import com.ruoyi.system.domain.VO.CmsUserVO;
import com.ruoyi.system.mapper.CmsUserMapper;
import com.ruoyi.system.service.ICmsUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * 接口实现
 * @author Penglei
 * @date 2020年6月4日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Primary
@Service
@Slf4j
public class CmsUserServiceImpl extends IBaseServiceImpl<CmsUser>
implements ICmsUserService {

    @Autowired
    private CmsUserMapper cmsUserMapper;

    @Autowired
    private WePayConfig wePayConfig;


    /**
     * 根据openid获取用户信息
     *
     * @param openid
     * @return CmsUserVO
     * @author Penglei
     * @date 2020年6月4日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public CmsUser selectUserByOpenId(String openid) {
        return  cmsUserMapper.selectOne(new QueryWrapper<CmsUser>().eq("open_id",openid));
    }

    /**
     * 删除信息
     *
     * @param ids
     * @return int
     * @author Penglei
     * @date 2020年5月26日15:52:39
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int deleteByIds(String ids) {
        return cmsUserMapper.deleteById(Convert.toStrArray(ids));
    }
}
