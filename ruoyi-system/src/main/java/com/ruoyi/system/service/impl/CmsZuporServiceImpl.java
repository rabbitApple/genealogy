package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.system.domain.CmsInfo;
import com.ruoyi.system.domain.CmsZupor;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.domain.VO.CmsZuporVO;
import com.ruoyi.system.mapper.CmsZuporMapper;
import com.ruoyi.system.mapper.SysFileInfoMapper;
import com.ruoyi.system.service.ICmsZuporService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 族谱操作接口实现
 * @author Penglei
 * @date  2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class CmsZuporServiceImpl extends IBaseServiceImpl<CmsZupor> implements ICmsZuporService {

    @Autowired
    private CmsZuporMapper cmsZuporMapper;

    @Autowired
    private SysFileInfoMapper sysFileInfoMapper;

    /**
     * 数据列表
     *
     * @param cmsZupor
     * @return int
     */
    @Override
    public List<CmsZupor> dataList(CmsZupor cmsZupor) {
        return cmsZuporMapper.selectInfoList(cmsZupor);
    }

    /**
     * 保存数据
     *
     * @param cmsZupor
     * @return int
     */
    @Override
    @Transactional(rollbackFor = Exception.class,isolation = Isolation.READ_COMMITTED)
    public int insert(CmsZupor cmsZupor) {
        int re = super.insert(cmsZupor);
        if(re > 0){
            SysFileInfo fileInfo =  sysFileInfoMapper.selectById(cmsZupor.getFileId());
            fileInfo.setInfoId(cmsZupor.getId());
            if(!fileInfo.getUid().equals(Integer.valueOf(DataConstants.NAGETIVE))){
                sysFileInfoMapper.updateById(fileInfo);
            }
        }
        return re;
    }

    /**
     * 删除信息
     * @param ids
     * @return int
     * @author Penglei
     * @date 2020年5月26日15:52:39
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int deleteByIds(String ids) {
        return cmsZuporMapper.deleteByIds(Convert.toStrArray(ids));
    }

    /**
     * 族谱列表，族谱案例列表
     * @param page
     * @param createBy , -1代表系统案例，数字代表具体用户ID
     * @return List
     * @author Penglei
     * @date 2020年6月5日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<CmsZupor> dataList(Integer page, String createBy) {
        IPage<CmsZupor> pages = new Page<>();
        pages.setCurrent(page);
        List<CmsZupor> datas = cmsZuporMapper
                .selectPage(pages,new QueryWrapper<CmsZupor>()
                        .eq("uid",createBy))
                .getRecords();
        datas.forEach(item->{
            item.setFilePath(sysFileInfoMapper.selectOne(new QueryWrapper<SysFileInfo>()
                    .eq("id",item.getFileId())).getFilePath());
        });
        return datas;
    }

    /**
     * 获取族谱详情
     *
     * @param id
     * @return CmsZupor
     * @author Penglei
     * @date 2020年6月5日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public CmsZuporVO getZuporDetail(Integer id) {
        return cmsZuporMapper.getZuporDetail(id);
    }
}
