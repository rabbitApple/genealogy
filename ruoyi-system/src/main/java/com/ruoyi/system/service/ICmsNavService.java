package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.system.domain.CmsMenu;

import java.util.List;

public interface ICmsNavService extends IBaseService<CmsMenu> {

    /**
     * 栏目列表
     *
     * @author Penglei
     * @date 2020年5月25日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    List<CmsMenu> selectMenuList(CmsMenu menu);


    /**
     * 根据栏目ID查询栏目
     * @author Penglei
     * @date 2020年5月26日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    CmsMenu selectMenuById(Integer id);


    public List<Ztree> menuTreeData(Integer id);

    public List<CmsMenu> selectMenuAll(Integer userId);


    /**
    * 查询是否存在下级
    * @author Penglei
    * @date 2020年5月26日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param menuId
    * @return boolean
    */
    boolean selectCountMenuByParentId(Long menuId);

    /**
    * 根据pid查询列表
    * @author Penglei
    * @date 2020年5月27日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param pid
    * @return  list
    */
    List<CmsMenu> selectListByPid(Integer pid);

}
