package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.mapper.SysFileInfoMapper;
import com.ruoyi.system.service.ISysFileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件信息接口实现
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年6月3日
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class SysFileInfoServiceImpl extends IBaseServiceImpl<SysFileInfo> implements ISysFileInfoService {

    @Autowired
    private SysFileInfoMapper sysFileInfoMapper;


    /**
     * 获取族谱封面列表
     *
     * @param sysFileInfo
     * @return List<SysFileInfo>
     * @author Penglei
     * @date 2020年6月3日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<SysFileInfo> selectFaceList(SysFileInfo sysFileInfo) {
        return sysFileInfoMapper.selectList(new QueryWrapper<SysFileInfo>().eq("keyss", sysFileInfo.getKeyss()));
    }

    /**
     * 获取指定文件
     *
     * @param sysFileInfo
     * @return List
     * @author Penglei
     * @date 2020年6月5日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<SysFileInfo> listByType(SysFileInfo sysFileInfo) {
        QueryWrapper query = new QueryWrapper<SysFileInfo>();
        if(!StringUtils.isNull(sysFileInfo.getKeyss())){
            query.eq("keyss", sysFileInfo.getKeyss());
        }
        if(!StringUtils.isNull(sysFileInfo.getUid())){
            query.eq("uid", sysFileInfo.getUid());
        }
        if(!StringUtils.isNull(sysFileInfo.getIds())){
            query.in("id",sysFileInfo.getIds().split(","));
        }
        if(!StringUtils.isNull(sysFileInfo.getInfoId())){
            query.eq("info_id", sysFileInfo.getInfoId());
        }
        return sysFileInfoMapper.selectList(query);
    }


    /**
     * 批量删除
     *
     * @param ids
     * @return int
     * @author Penglei
     * @date 2020年6月3日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int deleteByIds(String ids) {
        return sysFileInfoMapper.deleteByIds(Convert.toStrArray(ids));
    }


    /**
     * 查询实体文件信息
     * @param sysFileInfo
     * @returnw
     */
    @Override
    public SysFileInfo queryData(SysFileInfo sysFileInfo) {
        return sysFileInfoMapper.selectOne(new QueryWrapper<SysFileInfo>()
                .like("file_path", sysFileInfo.getFileName())
                .eq("keyss", sysFileInfo.getKeyss())
                .eq("uid", sysFileInfo.getUid()));
    }
}
