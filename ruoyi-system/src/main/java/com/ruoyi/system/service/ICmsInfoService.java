package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.CmsInfo;
import com.ruoyi.system.domain.CmsVI;
import com.ruoyi.system.domain.VO.CmsInfoVO;

import java.util.List;

/**
* 信息接口
* @author Penglei
* @date 2020年5月26日
* @version 1.0.0
*/
public interface ICmsInfoService extends IBaseService<CmsInfo> {

    /**
    * 信息管理列表
    * @author Penglei
    * @date 2020年5月26日14:06:27
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param cmsInfo
    * @return  CmsInfo
    */
    List<CmsInfoVO> selectList(CmsInfo cmsInfo);

    /**
    * 删除信息
    * @author Penglei
    * @date 2020年5月26日15:52:39
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param ids
    * @return  int
    */
    int deleteByIds(String ids);


    /**
    * 获取列表
    * @author Penglei
    * @date 2020年5月27日15:27:42
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param page
    * @param text
    * @return  List
    */
    List<CmsInfo> selectList(Integer page,String text);


    /**
    * 获取总数量
    * @author Penglei
    * @date 2020年5月27日16:57:37
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param nid
    * @return  int
    */
    int getCount(Integer nid);

    /**
    * 查询是否浏览过信息
    * @author Penglei
    * @date 2020年6月13日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param cmsVI
    * @return int
    */
    int queryViews(Integer infoId,Integer uid);

    int saveView(Integer infoId,Integer uid);
}
