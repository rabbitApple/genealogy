package com.ruoyi.system.service.impl;

import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.system.domain.CmsInfo;
import com.ruoyi.system.domain.CmsVI;
import com.ruoyi.system.domain.VO.CmsInfoVO;
import com.ruoyi.system.mapper.CmsInfoMapper;
import com.ruoyi.system.mapper.CmsVIMapper;
import com.ruoyi.system.service.ICmsInfoService;
import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * 信息接口实现
 * @author Penglei
 * @date  2020年5月26日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class CmsInfoServiceImpl extends IBaseServiceImpl<CmsInfo>
        implements ICmsInfoService {

    @Autowired
    private CmsInfoMapper cmsInfoMapper;

    @Autowired
    private CmsVIMapper cmsVIMapper;

    /**
     * 信息管理列表
     *
     * @param cmsInfo
     * @return CmsInfo
     * @author Penglei
     * @date 2020年5月26日14:06:27
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<CmsInfoVO> selectList(CmsInfo cmsInfo) {
        return cmsInfoMapper.selectInfoList(cmsInfo);
    }

    /**
     * 删除信息
     *
     * @param ids
     * @return int
     * @author Penglei
     * @date 2020年5月26日15:52:39
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int deleteByIds(String ids) {
        return cmsInfoMapper.deleteByIds(Convert.toStrArray(ids));
    }

    /**
     * 获取列表
     * @param page ： 搜索分页
     * @param text : 搜索字符
     * @return List
     * @author Penglei
     * @date 2020年5月27日15:27:42
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<CmsInfo> selectList(Integer page, String text) {
        IPage<CmsInfo> pages = new Page<>();
        pages.setCurrent(page);
        QueryWrapper query = new QueryWrapper<CmsInfo>();
        if(!text.equals(DataConstants.NAGETIVE)){
            query.like("title",text);
        }
        return cmsInfoMapper.selectPage(pages,query).getRecords();
    }

    /**
     * 获取总数量
     *
     * @param nid
     * @return int
     * @author Penglei
     * @date 2020年5月27日16:57:37
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int getCount(Integer nid) {
        return cmsInfoMapper.selectCount(new QueryWrapper<CmsInfo>().eq("nid",nid));
    }

    /**
     * 查询是否浏览过信息
     *
     * @param cmsVI
     * @return int
     * @author Penglei
     * @date 2020年6月13日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int queryViews(Integer infoId,Integer uid) {
        return cmsVIMapper.selectCount(new QueryWrapper<CmsVI>()
        .eq("info_id",infoId)
        .eq("uid",uid));
    }

    @Override
    public int saveView(Integer infoId, Integer uid) {
        CmsVI c = ReflectUtil.newInstance(CmsVI.class);
        c.setInfoId(infoId);
        c.setUid(uid);
        return cmsVIMapper.insert(c);
    }


    /**
     * 查询是否唯一
     * @param t
     * @return 0/1
     */
    @Override
    public String checkNameUnique(String t,Integer id) {
        QueryWrapper query = new QueryWrapper<CmsInfo>().eq("title",t);
        if(id != -1){
            query.ne("id",id);
        }
        return !Optional.ofNullable(cmsInfoMapper.selectOne(query)).isPresent()
                ? UserConstants.MENU_NAME_UNIQUE
                : UserConstants.MENU_NAME_NOT_UNIQUE;
    }
}
