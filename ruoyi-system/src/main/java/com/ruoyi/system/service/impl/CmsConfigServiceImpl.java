package com.ruoyi.system.service.impl;

import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.system.domain.CmsConfig;
import com.ruoyi.system.mapper.CmsConfigMapper;
import com.ruoyi.system.service.ICmsConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


/**
* 系统配置服务
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
@Primary
@Service
public class CmsConfigServiceImpl extends IBaseServiceImpl<CmsConfig>
        implements ICmsConfigService {

    @Autowired
    private CmsConfigMapper cmsConfigMapper;
}
