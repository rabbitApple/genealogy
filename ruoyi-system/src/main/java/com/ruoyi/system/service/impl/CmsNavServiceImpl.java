package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.CmsMenu;
import com.ruoyi.system.mapper.CmsNavMapper;
import com.ruoyi.system.service.ICmsNavService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 栏目业务实现
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年5月25日
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class CmsNavServiceImpl extends IBaseServiceImpl<CmsMenu> implements ICmsNavService {

    @Autowired
    private CmsNavMapper cmsNavMapper;

    /**
     * 栏目列表
     *
     * @param menu
     * @author Penglei
     * @date 2020年5月25日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<CmsMenu> selectMenuList(CmsMenu menu) {
        List<CmsMenu> list = cmsNavMapper.selectList(new QueryWrapper<>());
        return list;
    }

    /**
     * 根据栏目ID查询栏目
     *
     * @param id
     * @author Penglei
     * @date 2020年5月26日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public CmsMenu selectMenuById(Integer id) {
        return cmsNavMapper.selectMenuById(id);
    }


    /**
     * 对象转菜单树
     *
     * @param menuList     菜单列表
     * @param roleMenuList 角色已存在菜单列表
     * @param permsFlag    是否需要显示权限标识
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<CmsMenu> menuList, boolean permsFlag) {
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (CmsMenu menu : menuList) {
            Ztree ztree = new Ztree();
            ztree.setId(Long.valueOf(menu.getId()));
            ztree.setpId(Long.valueOf(menu.getPid()));
            ztree.setName(transMenuName(menu, permsFlag));
            ztree.setTitle(menu.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }

    /**
     * @author Penglei
     * @date
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    public String transMenuName(CmsMenu menu, boolean permsFlag) {
        StringBuffer sb = new StringBuffer();
        sb.append(menu.getName());
        return sb.toString();
    }


    /**
     * 查询所有菜单
     *
     * @return 菜单列表
     */
    @Override
    public List<Ztree> menuTreeData(Integer userId) {
        List<CmsMenu> menuList = selectMenuAll(userId);
        List<Ztree> ztrees = initZtree(menuList,false);
        return ztrees;
    }


    /**
     * 查询栏目集合
     * @return 所有菜单信息
     */
    @Override
    public List<CmsMenu> selectMenuAll(Integer userId) {
        List<CmsMenu> menuList = null;
        menuList = cmsNavMapper.selectList(new QueryWrapper<>());
        return menuList;
    }

    /**
     * 查询是否存在下级
     * @param menuId
     * @return boolean
     * @author Penglei
     * @date 2020年5月26日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public boolean selectCountMenuByParentId(Long menuId) {
        int count = cmsNavMapper.selectCount(new QueryWrapper<CmsMenu>().eq("pid",menuId));
        return count > 0 ? true : false;
    }

    /**
     * 根据pid查询列表
     *
     * @param pid
     * @return list
     * @author Penglei
     * @date 2020年5月27日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public List<CmsMenu> selectListByPid(Integer pid) {
        return cmsNavMapper.selectList(new QueryWrapper<CmsMenu>().eq("pid",pid));
    }


    /**
     * 查询是否唯一
     * @param t
     * @return
     */
    @Override
    public String checkNameUnique(String t,Integer id) {
        return !Optional.ofNullable(cmsNavMapper.selectOne(new QueryWrapper<CmsMenu>().eq("name", t))).isPresent()
                ? UserConstants.MENU_NAME_UNIQUE
                : UserConstants.MENU_NAME_NOT_UNIQUE;
    }
}
