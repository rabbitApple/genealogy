package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.CmsLinker;
import com.ruoyi.system.mapper.CmsLinkerMapper;
import com.ruoyi.system.service.ICmsLinkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * 友链服务接口实现
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年5月28日10:46:34
 */
@SuppressWarnings("ALL")
@Primary
@Service
public class CmsLinkerServiceImpl extends IBaseServiceImpl<CmsLinker>
        implements ICmsLinkerService {

    @Autowired
    private CmsLinkerMapper cmsLinkerMapper;

    /**
     * 数据列表
     *
     * @param cmsLinker
     * @return int
     */
    @Override
    public List<CmsLinker> dataList(CmsLinker cmsLinker) {
        return cmsLinkerMapper.selectList(new QueryWrapper<>());
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return int
     * @author Penglei
     * @date 2020年5月28日11:00:36
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public int deleteByIds(String ids) {
        return cmsLinkerMapper.deleteByIds(Convert.toStrArray(ids));
    }

    /**
     * 查询是否唯一
     *
     * @param t
     * @return 0/1
     */
    @Override
    public String checkNameUnique(String t,Integer id) {
        return !Optional.ofNullable(cmsLinkerMapper.selectOne(new QueryWrapper<CmsLinker>().eq("name", t))).isPresent()
                ? UserConstants.MENU_NAME_UNIQUE
                : UserConstants.MENU_NAME_NOT_UNIQUE;
    }
}
