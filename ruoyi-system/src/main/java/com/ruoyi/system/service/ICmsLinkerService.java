package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.CmsLinker;

/**
 * 友链服务接口
 * @author Penglei
 * @date 2020年5月28日10:45:43
 * @version 1.0.0
 */
public interface ICmsLinkerService extends IBaseService<CmsLinker> {

    /**
    * 批量删除
    * @author Penglei
    * @date 2020年5月28日11:00:36
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param ids
    * @return int
    */
    int deleteByIds(String ids);
}
