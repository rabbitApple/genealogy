package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.CmsZuporPeople;
import com.ruoyi.system.domain.VO.CmsZuporPeopleVO;
import com.ruoyi.system.domain.VO.PeopleTreeVO;

import java.util.List;
import java.util.Optional;

/**
 * 族人服务接口
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年6月8日
 * @updateVersion 1.0.0
 */
public interface ICmsZuporPeopleService extends IBaseService<CmsZuporPeople> {
    /**
     * 获取族人
     * @param id
     * @return List<CmsZuporPeople>
     * @author Penglei
     * @date 2020年6月9日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    Optional<CmsZuporPeopleVO> dataTreeById(Integer id);
    /**
     * 获取族人树
     * @param id
     * @return List<CmsZuporPeople>
     * @author Penglei
     * @date 2020年6月9日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    List<PeopleTreeVO> dataTree(Integer id);

    /**
    * 获取族人当前有几代人
    * @author Penglei
    * @date 2020年6月9日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param id
    * @return  CmsZuporPeople
    */
    CmsZuporPeople getPeopleLevel(Integer id);
}
