package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.CmsOrder;

import javax.servlet.http.HttpServletRequest;

/**
 * 订单服务接口
 * @author Penglei
 * @date 2020年6月9日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface IOrderService extends IBaseService<CmsOrder> {


    /**
    * 统一下单
    * @author Penglei
    * @date 2020年6月9日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param orderNo
    * @return  AjaxResult
    */
    AjaxResult unifiedOrder(String orderNo, String openid);

    /**
    * 微信回调
    * @author Penglei
    * @date 2020年6月9日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param request
    * @return  String
    */
    String notify(HttpServletRequest request) throws Exception;
}
