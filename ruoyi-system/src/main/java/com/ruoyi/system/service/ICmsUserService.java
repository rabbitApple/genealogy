package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.CmsUser;

/**
 * 用户管理服务接口
 * @author Penglei
 * @date 2020年6月4日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface ICmsUserService extends IBaseService<CmsUser> {


    /**
    * 根据openid获取用户信息
    * @author Penglei
    * @date 2020年6月4日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param openid
    * @return  CmsUserVO
    */
    CmsUser selectUserByOpenId(String openid);




    /**
     * 删除信息
     * @author Penglei
     * @date 2020年5月26日15:52:39
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param ids
     * @return  int
     */
    int deleteByIds(String ids);
}
