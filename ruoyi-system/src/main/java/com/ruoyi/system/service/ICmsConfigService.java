package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.CmsConfig;

/**
* 内容管理系统配置类
* @author Penglei
* @date 2020年5月25日
*/
public interface ICmsConfigService extends IBaseService<CmsConfig>  {
}
