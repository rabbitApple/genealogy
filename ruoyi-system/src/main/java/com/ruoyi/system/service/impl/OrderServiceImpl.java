package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayUtil;
import com.ruoyi.common.base.service.impl.IBaseServiceImpl;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.common.pay.wepay.WePayConfig;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.CmsMemberType;
import com.ruoyi.system.domain.CmsOrder;
import com.ruoyi.system.mapper.CmsMemberMapper;
import com.ruoyi.system.mapper.CmsOrderMapper;
import com.ruoyi.system.mapper.CmsUserMapper;
import com.ruoyi.system.service.IOrderService;
import com.ruoyi.system.strategy.member.MemberContext;
import com.ruoyi.system.strategy.member.impl.Monthly;
import com.ruoyi.system.strategy.member.impl.Quarter;
import com.ruoyi.system.strategy.member.impl.Years;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * 订单服务实现
 *
 * @author Penglei
 * @version 1.0.0
 * @date
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@Primary
@Service
@Slf4j
public class OrderServiceImpl extends IBaseServiceImpl<CmsOrder>
        implements IOrderService {

    @Autowired
    private WePayConfig wePayConfig;

    @Autowired
    private CmsOrderMapper cmsOrderMapper;

    @Autowired
    private CmsUserMapper cmsUserMapper;

    @Autowired
    private CmsMemberMapper cmsMemberMapper;

    /**
     * 统一下单
     *
     * @param orderNo
     * @return AjaxResult
     * @author Penglei
     * @date 2020年6月9日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public AjaxResult unifiedOrder(String orderNo, String openid) {
        if (StringUtils.isEmpty(orderNo)) {
            return AjaxResult.error("支付失败，不存在订单号");
        }
        CmsOrder cmsOrder = cmsOrderMapper.selectOne(new QueryWrapper<CmsOrder>().eq("order_no", orderNo));
        Map<String, String> returnMap = new LinkedHashMap(1024);
        Map<String, String> responseMap = new HashMap(1024);
        HashMap requestMap = new HashMap(1024);
        try {
            WXPay wxPay = new WXPay(this.wePayConfig);
            requestMap.put("body", "会员购买叠加");
            requestMap.put("out_trade_no", orderNo);
            requestMap.put("total_fee", String.valueOf((int) (cmsOrder.getPrice().doubleValue() * 100.0D)));
            requestMap.put("spbill_create_ip", IpUtils.getHostIp());
            requestMap.put("openid", openid);
            requestMap.put("trade_type", "JSAPI");
            log.info("回调地址：" + this.wePayConfig.getPayNotifyUrl());
            requestMap.put("notify_url", this.wePayConfig.getPayNotifyUrl());
            new HashMap(1024);
            Map<String, String> resultMap = wxPay.unifiedOrder(requestMap);
            log.debug(requestMap.toString());
            String returnCode = resultMap.get("return_code");
            String returnMsg = resultMap.get("return_msg");
            String timestamp;
            if (DataConstants.SUCCESS.equals(returnCode)) {
                String resultCode = resultMap.get("result_code");
                timestamp = resultMap.get("err_code_des");
                if (DataConstants.SUCCESS.equals(resultCode)) {
                    responseMap = resultMap;
                }
            }

            if (responseMap != null && !(responseMap).isEmpty()) {
                Long time = System.currentTimeMillis() / 1000L;
                timestamp = time.toString();
                returnMap.clear();
                returnMap.put("appId", this.wePayConfig.getAppID());
                returnMap.put("nonceStr", responseMap.get("nonce_str"));
                returnMap.put("package", "Sign=WXPay");
                returnMap.put("signType", "MD5");
                returnMap.put("package", "prepay_id=" + ((Map) responseMap).get("prepay_id"));
                returnMap.put("timeStamp", timestamp);
                returnMap.put("sign", WXPayUtil.generateSignature(returnMap, this.wePayConfig.getKey()));
                log.debug(returnMap.toString());
                return AjaxResult.success(returnMap);
            } else {
                return AjaxResult.error("获取预支付交易会话标识失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 微信回调
     *
     * @param request
     * @return String
     * @author Penglei
     * @date 2020年6月9日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @Override
    public String notify(HttpServletRequest request) throws Exception {
        Map<String, String> map = new HashMap();
        System.out.println("----------------微信回调开始啦----------------------");
        StringBuffer sb = new StringBuffer();
        InputStream inputStream = request.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, DataConstants.CHARSET));
        String s;
        while ((s = in.readLine()) != null) {
            sb.append(s);
        }

        in.close();
        inputStream.close();
        log.debug("sb:{}", sb);
        new HashMap();
        Map<String, String> m = WXPayUtil.xmlToMap(sb.toString());
        SortedMap<Object, Object> packageParams = new TreeMap();
        Iterator it = m.keySet().iterator();

        String out_trade_no;
        String user;
        while (it.hasNext()) {
            out_trade_no = (String) it.next();
            String parameterValue = m.get(out_trade_no);
            user = "";
            if (null != parameterValue) {
                user = parameterValue.trim();
            }

            log.info("p:" + out_trade_no + ",v:" + user);
            packageParams.put(out_trade_no, user);
        }
        if (DataConstants.SUCCESS.equals(packageParams.get(DataConstants.RESULT_CODE))) {
            out_trade_no = (String) packageParams.get("out_trade_no");
            if (!StringUtils.isEmpty(out_trade_no)) {
                CmsOrder order = cmsOrderMapper.selectOne((new QueryWrapper<CmsOrder>())
                        .eq("order_no", out_trade_no)
                        .in("status","1"));
                if (!StringUtils.isNull(order)) {
                    order.setStatus(6);
                    cmsOrderMapper.updateById(order);
//                 会员加期
//                 获取会员金额
                    Optional<CmsMemberType> memberType = Optional.ofNullable(cmsMemberMapper.selectById(order.getMid()));
                    MemberContext context = null;
                    if(memberType.isPresent()){
                        if(memberType.get().getPrice() >= DataConstants.YEAR){
                            context = new MemberContext(new Years());
                        }else if(memberType.get().getPrice() >= DataConstants.QUARTER){
                            context = new MemberContext(new Quarter());
                        }else if(memberType.get().getPrice() >= DataConstants.MONTHLY){
                            context = new MemberContext(new Monthly());
                        }
                        context.appendTime(order.getUid(),cmsUserMapper);
                        log.info("微信手机支付回调成功,订单号:{}", out_trade_no);
                        map.put("return_code", DataConstants.SUCCESS);
                        map.put("return_msg", DataConstants.OKS);
                        return WXPayUtil.mapToXml(map);
                    }
                }
            }
        }
        map.put("return_code", "error");
        map.put("return_msg", "支付失败");
        return WXPayUtil.mapToXml(map);
    }
}
