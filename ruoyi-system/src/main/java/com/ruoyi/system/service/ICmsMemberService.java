package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.CmsMemberType;

/**
 * 会员类型说明服务接口
 * @author Penglei
 * @date  2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface ICmsMemberService extends IBaseService<CmsMemberType> {

    /**
     * 删除信息
     * @author Penglei
     * @date 2020年5月26日15:52:39
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param ids
     * @return  int
     */
    int deleteByIds(String ids);
}
