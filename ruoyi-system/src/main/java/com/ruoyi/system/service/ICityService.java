package com.ruoyi.system.service;

import com.ruoyi.common.base.service.IBaseService;
import com.ruoyi.system.domain.City;

import java.util.List;

/**
 * 城市服务接口
 * @author Penglei
 * @date 2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public interface ICityService extends IBaseService<City> {

    public List<City> dataList(Integer city);
}
