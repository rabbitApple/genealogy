package com.ruoyi.web.controller.cms;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CmsZupor;
import com.ruoyi.system.domain.VO.CmsZuporPeopleVO;
import com.ruoyi.system.domain.VO.PeopleTreeVO;
import com.ruoyi.system.service.ICmsZuporPeopleService;
import com.ruoyi.system.service.ICmsZuporService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/cms/zupor")
@Slf4j
public class ZuporController extends BaseController {

    private String prefix = "cms/zupor/";

    @Autowired
    private ICmsZuporService cmsZuporService;

    @Autowired
    private ICmsZuporPeopleService cmsZuporPeopleService;

//    ------------------------------------------------------------视图定向区-----------------------------

    @RequiresPermissions("cms:zupor:view")
    @GetMapping()
    public String index() {
        return prefix + "index";
    }


    @RequiresPermissions("cms:zupor:add")
    @GetMapping("/add")
    public String add() {
        return prefix + "add";
    }


    @RequiresPermissions("cms:zupor:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap modelMap) {
        modelMap.put("info", cmsZuporService.getDataById(id));
        return prefix + "edit";
    }

    @RequiresPermissions("cms:zupor:lookpeople")
    @GetMapping("/lookPeople/{id}")
    public String lookPeople(@PathVariable("id") Integer id, ModelMap map) {
        List<PeopleTreeVO> list = cmsZuporPeopleService.dataTree(id);
        map.put("list", list);
        return prefix + "look";
    }


//   --------------------------------------------------------------数据操作区--------------------------


    /**
     * 族谱案例列表
     *
     * @param cmsZupor
     * @return TableDataInfo
     * @author Penglei
     * @date 2020年6月4日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @RequiresPermissions("cms:zupor:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsZupor cmsZupor) {
        startPage();
        return getDataTable(cmsZuporService.dataList(cmsZupor));
    }


    /**
     * 保存族谱案例
     *
     * @param cmsZupor
     * @return AjaxResult
     * @author Penglei
     * @date 2020年6月4日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @RequiresPermissions("cms:zupor:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(CmsZupor cmsZupor) {
        return toAjax(cmsZuporService.insert(cmsZupor));
    }

    /**
     * 修改族谱
     *
     * @param cmsZupor
     * @return AjaxResult
     * @author Penglei
     * @date 2020年6月4日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @RequiresPermissions("cms:zupor:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(CmsZupor cmsZupor) {
        return toAjax(cmsZuporService.edit(cmsZupor));
    }


    /**
     * 批量删除
     */
    @RequiresPermissions("cms:zupor:remove")
    @Log(title = "批量删除", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(cmsZuporService.deleteByIds(ids));
    }


}
