package com.ruoyi.web.controller.cms;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CmsUser;
import com.ruoyi.system.service.ICmsUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 用户控制
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年6月4日
 * @updateVersion 1.0.0
 */
@Controller
@RequestMapping("/cms/user")
public class UserController extends BaseController {

    private String prefix = "cms/user/";

    @Autowired
    private ICmsUserService cmsUserService;

    //    ---------------------------------------------------------转向控制----------------------------------
    @RequiresPermissions("cms:user:view")
    @GetMapping()
    public String index() {
        return prefix + "index";
    }

    @RequiresPermissions("cms:user:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap modelMap) {
        modelMap.put("info", cmsUserService.getDataById(id));
        return prefix + "edit";
    }
//    --------------------------------------------------------操作控制-----------------------------------

    @RequiresPermissions("cms:user:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsUser cmsUser) {
        startPage();
        List<CmsUser> list = cmsUserService.dataList(cmsUser);
        return getDataTable(list);
    }

    /**
     * 修改用户信息
     *
     * @param cmsUser
     * @return AjaxResult
     * @author Penglei
     * @date 2020年6月4日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @RequiresPermissions("cms:user:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(CmsUser cmsUser) {
        return toAjax(cmsUserService.edit(cmsUser));
    }


    /**
     * 批量删除
     */
    @RequiresPermissions("cms:info:remove")
    @Log(title = "批量删除", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(cmsUserService.deleteByIds(ids));
    }


}
