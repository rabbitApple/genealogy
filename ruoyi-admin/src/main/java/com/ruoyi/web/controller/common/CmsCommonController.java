//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ruoyi.web.controller.common;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.AjaxResult.Type;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.service.ISysFileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

/**
 * 通用文件上传（前端）
 * @author Penglei
 * @date 2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@Controller
@RequestMapping({"/api/common"})
@CrossOrigin({"*"})
public class CmsCommonController extends BaseController {
    private String filePath;

    @Autowired
    private ISysFileInfoService sysFileInfoService;

    /**
    * 不进数据库上传文件
    * @author Penglei
    * @date 2020年6月3日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param file
    */
    @Log(
            title = "文件上传插件",
            businessType = BusinessType.INSERT
    )
    @PostMapping({"/uploadFileMulti"})
    @ResponseBody
    public AjaxResult uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
            this.filePath = FileUploadUtils.upload(file);
            return AjaxResult.success("上传成功",filePath);
    }






    /**
    * 上传文件保存方法，进数据库
    * @author Penglei
    * @date 2020年6月3日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param file
    * @param fileInfo
    * @return
    */
    @PostMapping("/saveFileData")
    @ResponseBody
    public AjaxResult addSave(@RequestParam("file") MultipartFile file, SysFileInfo fileInfo) throws IOException
    {
//      查询
        if(file.getOriginalFilename().length() <= 45){
            fileInfo.setFileName(file.getOriginalFilename());
        }
        SysFileInfo f =  sysFileInfoService.queryData(fileInfo);
        if(Optional.ofNullable(f).isPresent()){
            return success("文件上传成功",f.getId());
        }
        // 上传并返文件名称
        String fileName = FileUploadUtils.upload(file);
        fileInfo.setFilePath(fileName);
        int id = sysFileInfoService.insert(fileInfo);
        return id> 0
                ? success("文件上传成功",sysFileInfoService.queryData(fileInfo).getId())
                : error("文件上传失败");
    }
}
