package com.ruoyi.web.controller.api;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.VO.CmsZuporPeopleVO;
import com.ruoyi.system.domain.VO.CmsZuporVO;
import com.ruoyi.system.service.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 首页api接口
 * 本接口不想分接口写，将基本接口融合到一个方法，用户除外
 * 尽管会造成代码臃肿，哎，不想单独写控制器
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年6月4日
 * @updateVersion 1.0.0
 */
@Controller
@RequestMapping("/api/index")
@Api(tags = "普通API接口")
public class ApiIndexController extends BaseController {

    @Autowired
    private ICmsInfoService cmsInfoService;

    @Autowired
    private ICmsZuporService cmsZuporService;

    @Autowired
    private ICmsConfigService cmsConfigService;

    @Autowired
    private ISysDictDataService sysDictDataService;

    @Autowired
    private ISysFileInfoService sysFileInfoService;

    @Autowired
    private ICityService cityService;

    @Autowired
    private ICmsZuporPeopleService cmsZuporPeopleService;

    @Autowired
    private ICmsMemberService cmsMemberService;

    /**
     * 首页资讯列表及搜索
     *
     * @param page : 分页
     * @param text : 搜索字符
     * @return AjaxResult
     * @author Penglei
     * @date
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @ApiOperation(value = "首页资讯列表及搜索")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @GetMapping("/search/{page}/{text}")
    @ResponseBody
    public AjaxResult search(
            @ApiParam(value = "分页参数", name = "page", required = true) @PathVariable("page") Integer page,
            @ApiParam(value = "搜索参数，-1代表全部", name = "text") @PathVariable("text") String text) {
        List<CmsInfo> infos = cmsInfoService.selectList(page, text);
        return infos.size() > 0
                ? success(DataConstants.QUERY_DATA_SUCCESS, infos)
                : error(DataConstants.QUERY_DATA_ISNULL);
    }

    @ApiOperation(value = "获取资讯详情")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @GetMapping("/getNewDetail/{id}/{uid}")
    @ResponseBody
    public AjaxResult getNewDetail(@PathVariable("id") Integer id, @PathVariable("uid") Integer uid) {
        CmsInfo cmsInfo = cmsInfoService.getDataById(id);
        int views = cmsInfoService.queryViews(id, uid);
        if (Optional.ofNullable(cmsInfo).isPresent() && views == 0) {
//         插入
            if (uid != -1) {
                cmsInfo.setViews(cmsInfo.getViews() + 1);
                cmsInfoService.edit(cmsInfo);
                cmsInfoService.saveView(id, uid);
            }
        }
        return Optional.ofNullable(cmsInfo).isPresent()
                ? success(DataConstants.QUERY_DATA_SUCCESS, cmsInfo)
                : error(DataConstants.QUERY_DATA_ISNULL);
    }

//-----------------------------------------------------------------------族谱-----------------------------------

    /**
     * 获取案例鉴赏列表
     *
     * @param page :分页参数
     * @return AjaxResult
     * @author Penglei
     * @date 2020年6月5日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @ApiOperation(value = "族谱案例列表")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @GetMapping("/caseApp/{page}/{createBy}")
    @ResponseBody
    public AjaxResult caseApp(
            @ApiParam(name = "page", value = "分页", required = true) @PathVariable("page") Integer page,
            @ApiParam(name = "createBy", value = "创建者", required = true) @PathVariable("createBy") String createBy) {
        List<CmsZupor> zupors = cmsZuporService.dataList(page, createBy);
        return zupors.size() > 0
                ? success(DataConstants.QUERY_DATA_SUCCESS, zupors)
                : error(DataConstants.QUERY_DATA_ISNULL);
    }

    /**
     * 族谱详情
     *
     * @param id
     * @author Penglei
     * @date 2020年6月5日
     * @version 1.0.0
     * @updateVersion 1.0.0
     */
    @ApiOperation(value = "获取族谱详情", response = CmsZupor.class)
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @GetMapping("/zuporDetail/{id}")
    @ResponseBody()
    public AjaxResult zuporDetail(
            @ApiParam(name = "id", value = "族谱ID", required = true)
            @PathVariable("id") Integer id) {
        CmsZuporVO cmsZuporVO = cmsZuporService.getZuporDetail(id);
        return Optional.ofNullable(cmsZuporVO).isPresent()
                ? success(DataConstants.QUERY_DATA_SUCCESS, cmsZuporVO)
                : error(DataConstants.QUERY_DATA_ISNULL);
    }

    @ApiOperation(value = "新增族谱")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.SAVE_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.SAVE_FIAL),
    })
    @PostMapping("/addZupor")
    @ResponseBody
    public AjaxResult addZupor(CmsZupor cmsZupor) {
        return toAjax(cmsZuporService.insert(cmsZupor));
    }


    @ApiOperation(value = "更新族谱")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.UPDATE_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.UPDATE_FIELD),
    })
    @PostMapping("/updateZupor")
    @ResponseBody
    public AjaxResult updateZupor(CmsZupor cmsZupor) {
        return toAjax(cmsZuporService.edit(cmsZupor));
    }

//-------------------------------------------------------族人----------------------------------------------

    @ApiOperation(value = "添加族人")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.SAVE_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.SAVE_FIAL),
    })
    @PostMapping("/addPeople")
    @ResponseBody
    public AjaxResult addPeople(CmsZuporPeople zuporPeople) {
        int ret = cmsZuporPeopleService.insert(zuporPeople);
        return ret>0
                ?success(DataConstants.SAVE_SUCCESS,ret)
                :error( DataConstants.SAVE_FIAL);
    }


    @ApiOperation(value = "删除族人")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.REMOVE_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.REMOVE_FAIL),
    })
    @GetMapping("/delPeople/{id}")
    @ResponseBody
    public AjaxResult delPeople(@PathVariable("id") Integer id) {
        return cmsZuporPeopleService.delete(id) > 0
                ? success(DataConstants.REMOVE_SUCCESS)
                : error(DataConstants.REMOVE_FAIL + "，此人存在子嗣,不能删除");
    }


    @ApiOperation(value = "获取族人，根据世代排序")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @GetMapping("/getPeople/{id}")
    @ResponseBody
    public AjaxResult getPeople(@ApiParam(value = "族谱ID", name = "id")
                                @PathVariable("id") Integer id) {
        Optional<CmsZuporPeopleVO> list = cmsZuporPeopleService.dataTreeById(id);
        return list.isPresent()
                ? success(DataConstants.QUERY_DATA_SUCCESS, list.get())
                : error(DataConstants.QUERY_DATA_ISNULL);
    }

    @ApiOperation(value = "获取族人有几代")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @GetMapping("/getPeopleLevel/{id}")
    @ResponseBody
    public AjaxResult getPeopleLevel(@ApiParam(value = "族谱ID", name = "id")
                                     @PathVariable("id") Integer id) {
        CmsZuporPeople c = cmsZuporPeopleService.getPeopleLevel(id);
        return success(DataConstants.QUERY_DATA_SUCCESS
                , StringUtils.isNull(c) ? 0 : c.getGeneration());
    }


    //--------------------------------------------------------获取会员等级------------------------------------------
    @ApiOperation(value = "会员说明")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @GetMapping("/memberDlist")
    @ResponseBody
    public AjaxResult memberDlist() {
        Optional<List<CmsMemberType>> memberTypes = Optional.ofNullable(cmsMemberService.dataList(new CmsMemberType()));
        return memberTypes.isPresent()
                ? success(DataConstants.QUERY_DATA_SUCCESS, memberTypes.get())
                : error(DataConstants.QUERY_DATA_ISNULL);
    }


//----------------------------------------------------------杂项-------------------------------------------------

    @ApiOperation(value = "获取系统配置")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS)
    })
    @GetMapping("/getConfig")
    @ResponseBody
    public AjaxResult getConfig() {
        return success(DataConstants.QUERY_DATA_SUCCESS, cmsConfigService.getDataById(1));
    }


    @ApiOperation(value = "获取世代")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.QUERY_DATA_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.QUERY_DATA_ISNULL),
    })
    @ResponseBody
    @GetMapping("/getShi/{type}")
    public AjaxResult getShi(
            @ApiParam(name = "type", value = "字典值", required = true)
            @PathVariable("type")
                    String type) {
        List<SysDictData> dictDatas = sysDictDataService.selectDictDataByType(type);
        return dictDatas.size() > 0
                ? success(DataConstants.QUERY_DATA_SUCCESS, dictDatas)
                : error(DataConstants.QUERY_DATA_ISNULL);
    }

    @ApiOperation(value = "获取文件")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "文件类型", name = "keyss"),
            @ApiImplicitParam(value = "对应信息ID", name = "infoId"),
            @ApiImplicitParam(value = "所属用户", name = "uid"),
            @ApiImplicitParam(value = "所属用户ID集合", name = "ids"),
    })
    @PostMapping("/getFace")
    @ResponseBody
    public AjaxResult getFace(SysFileInfo sysFileInfo) {
        return success(DataConstants.QUERY_DATA_SUCCESS, sysFileInfoService.listByType(sysFileInfo));
    }

    @ApiOperation(value = "根据文件ID获取文件")
    @GetMapping("/getFaceById/{id}")
    @ResponseBody
    public AjaxResult getFaceById(@PathVariable("id") Integer id) {
        SysFileInfo fileInfo = sysFileInfoService.getDataById(id);
        return Optional.ofNullable(fileInfo).isPresent() ?
                success(DataConstants.QUERY_DATA_SUCCESS, fileInfo)
                : error();
    }


    @ApiOperation(value = "获取城市")
    @GetMapping("/getCity/{parentId}")
    @ResponseBody
    public AjaxResult getCity(
            @ApiParam(value = "上级城市码", name = "parentId", required = true)
            @PathVariable("parentId")
                    String parentId) {
        return success(DataConstants.QUERY_DATA_SUCCESS, cityService.dataList(Integer.parseInt(parentId)));
    }


//    @GetMapping("/getUser")
//    @ResponseBody
//    public String getUser() throws Exception {
//        CmsUser cmsUser = new CmsUser();
//        cmsUser.setNickName("彭磊");
//        cmsUser.setUserPhone("15925884562");
//        return SecurityUtil.encrypt(JSONUtil.parse(success(DataConstants.QUERY_DATA_SUCCESS, cmsUser)).toString());
//    }
}
