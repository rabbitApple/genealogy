package com.ruoyi.web.controller.cms;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CmsMenu;
import com.ruoyi.system.service.ICmsNavService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
* 导航控制
* @author Penglei
* @date 2020年5月25日
* @version 1.0.0
*/
@Controller
@RequestMapping("/cms/nav")
public class NavController extends BaseController {

    private String prefix = "cms/menu/";

    @Autowired
    private ICmsNavService cmsNavService;

    @RequiresPermissions("cms:nav:view")
    @GetMapping()
    public String index()
    {
        return  prefix + "index";
    }


    @RequiresPermissions("cms:nav:list")
    @PostMapping("/list")
    @ResponseBody
    public List<CmsMenu> list(CmsMenu menu)
    {
        List<CmsMenu> menuList = cmsNavService.selectMenuList(menu);
        return menuList;
    }




    /**
     * 新增
     */
    @GetMapping("/add/{pid}")
    public String add(@PathVariable("pid") Integer parentId, ModelMap mmap)
    {
        CmsMenu menu = null;
        if (0L != parentId)
        {
            menu = cmsNavService.getDataById(parentId);
        }
        else
        {
            menu = new CmsMenu();
            menu.setId(0);
            menu.setName("主目录");
        }
        mmap.put("menu", menu);
        return prefix + "add";
    }

    /**
     * 新增保存菜单
     */
    @Log(title = "栏目管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("cms:nav:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated CmsMenu menu)
    {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(cmsNavService.checkNameUnique(menu.getName(),-1)))
        {
            return error("新增栏目'" + menu.getName() + "'失败，栏目名称已存在");
        }
        return toAjax(cmsNavService.insert(menu));
    }


    /**
     * 修改菜单
     */
    @GetMapping("/edit/{menuId}")
    public String edit(@PathVariable("menuId") Integer menuId, ModelMap mmap)
    {
        mmap.put("menu",cmsNavService.selectMenuById(menuId));
        return prefix + "edit";
    }

    /**
     * 修改保存菜单
     */
    @Log(title = "栏目管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("cms:nav:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated CmsMenu menu)
    {
//        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(cmsNavService.checkNameUnique(menu.getName())))
//        {
//            return error("修改栏目'" + menu.getName() + "'失败，栏目名称已存在");
//        }
        return toAjax(cmsNavService.edit(menu));
    }


    /**
     * 删除栏目
     */
    @Log(title = "栏目管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("cms:nav:remove")
    @GetMapping("/remove/{menuId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("menuId") Long menuId)
    {
        if (cmsNavService.selectCountMenuByParentId(menuId))
        {
            return AjaxResult.warn("存在子菜单,不允许删除，请先删除子栏目");
        }
        return toAjax(cmsNavService.delete(menuId.intValue()));
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/selectMenuTree/{menuId}")
    public String selectMenuTree(@PathVariable("menuId") Integer menuId, ModelMap mmap)
    {
        mmap.put("menu", cmsNavService.getDataById(menuId));
        return prefix + "tree";
    }


    /**
     * 加载所有菜单列表树
     */
    @GetMapping("/menuTreeData")
    @ResponseBody
    public List<Ztree> menuTreeData()
    {
        List<Ztree> ztrees = cmsNavService.menuTreeData(-1);
        return ztrees;
    }

}
