package com.ruoyi.web.controller.cms;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.framework.web.domain.server.SysFile;
import com.ruoyi.system.domain.SysFileInfo;
import com.ruoyi.system.service.ISysFileInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 族谱封面管理
 * @author Penglei
 * @date 2020年6月3日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@Controller
@RequestMapping("/cms/zuporface")
@Slf4j
public class ZuporFaceController extends BaseController {

    private String prefix = "cms/zuporface/";

    @Autowired
    private ISysFileInfoService sysFileInfoService;



//    ------------------------------------------视图跳转区---------------------------------------
    @RequiresPermissions("cms:zuporface:view")
    @GetMapping()
    public String index()
    {
        return prefix+"index";
    }


    @RequiresPermissions("cms:zuporface:view")
    @GetMapping("/add")
    public String add()
    {
        return prefix+"add";
    }


    @RequiresPermissions("cms:zuporface:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap modelMap)
    {
        modelMap.put("files",sysFileInfoService.getDataById(id));
        return prefix+"edit";
    }




//    ---------------------------------------------------数据操作区--------------------------------
    /**
    * 封面列表
    * @author Penglei
    * @date 2020年6月3日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param sysFileInfo
    * @return  TableDataInfo
    */
    @RequiresPermissions("cms:zuporface:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysFileInfo sysFileInfo)
    {
        startPage();
        List<SysFileInfo> list =  sysFileInfoService.selectFaceList(sysFileInfo);
        return getDataTable(list);
    }



    /**
    * 保存封面文件信息
    * @author Penglei
    * @date 2020年6月3日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param sysFileInfo
    * @return  AjaxResult
    */
    @RequiresPermissions("cms:zuporface:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(SysFileInfo sysFileInfo)
    {
        return toAjax(sysFileInfoService.insert(sysFileInfo));
    }


    /**
     * 保存封面文件信息
     * @author Penglei
     * @date 2020年6月3日
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param sysFileInfo
     * @return  AjaxResult
     */
    @RequiresPermissions("cms:zuporface:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(SysFileInfo sysFileInfo)
    {
        return toAjax(sysFileInfoService.edit(sysFileInfo));
    }


    /**
     * 删除封面
     */
    @RequiresPermissions("cms:info:remove")
    @Log(title = "批量删除", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysFileInfoService.deleteByIds(ids));
    }


}
