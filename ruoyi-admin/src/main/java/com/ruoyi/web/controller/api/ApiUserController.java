package com.ruoyi.web.controller.api;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.finals.DataConstants;
import com.ruoyi.common.utils.OpenIdUtil;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import com.ruoyi.common.utils.security.SecurityUtil;
import com.ruoyi.system.domain.CmsMemberType;
import com.ruoyi.system.domain.CmsOrder;
import com.ruoyi.system.domain.CmsUser;
import com.ruoyi.system.service.ICmsMemberService;
import com.ruoyi.system.service.ICmsUserService;
import com.ruoyi.system.service.IOrderService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * 用户接口控制
 * @author Penglei
 * @date 2020年6月4日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@SuppressWarnings("ALL")
@RestController
@RequestMapping("/api/user")
@Slf4j
@Api(tags = "用户相关API接口")
public class ApiUserController extends BaseController {

    @Autowired
    private OpenIdUtil openIdUtil;

    @Autowired
    private ICmsUserService cmsUserService;

    @Autowired
    private ICmsMemberService cmsMemberService;

    @Autowired
    private IOrderService orderService;

    private  CmsUser cmsUser;
//    --------------------------------------------------------我是操作区--------------------------------------------------

    @ApiOperation(value = "获取用户openid")
    @GetMapping("/getOpenId/{code}")
    @ResponseBody
    public AjaxResult getOpenId(@PathVariable("code") String code)
    {
        return success(DataConstants.QUERY_DATA_SUCCESS,openIdUtil.getOpenId(code));
    }


    @ApiOperation(value = "保存用户信息-注册用户")
    @PostMapping("/saveUser")
    @ResponseBody
    public  AjaxResult saveUser(String user) throws Exception {
        cmsUser = JSONUtil.toBean(SecurityUtil.desEncrypt(user),CmsUser.class);
        CmsUser cmsUserVO = cmsUserService.selectUserByOpenId(cmsUser.getOpenId());
        if(Optional.ofNullable(cmsUserVO).isPresent()){
            return success(DataConstants.QUERY_DATA_SUCCESS,SecurityUtil.encrypt(JSONUtil.parse(cmsUserVO).toString()));
        }
        return cmsUserService.insert(cmsUser) > 0
                ? success(DataConstants.REGISTER_SUCCESS,SecurityUtil.encrypt(JSONUtil.parse(cmsUser).toString()))
                : error(DataConstants.REGISTER_FIELD);
    }


    @ApiOperation(value = "更新用户信息")
    @PostMapping("/updateUser")
    @ResponseBody
    public  AjaxResult updateUser(String user) throws Exception {
        cmsUser = JSONUtil.toBean(SecurityUtil.desEncrypt(user),CmsUser.class);
        CmsUser cmsUserVO = cmsUserService.selectUserByOpenId(cmsUser.getOpenId());
        if(!Optional.ofNullable(cmsUserVO).isPresent()){
            return error(DataConstants.NOT_USER);
        }
        cmsUserVO.setNickName(cmsUser.getNickName());
        cmsUserVO.setUserPhone(cmsUser.getUserPhone());
        return cmsUserService.edit(cmsUserVO) > 0
                ? success(DataConstants.UPDATE_SUCCESS, SecurityUtil.encrypt(JSONUtil.parse(cmsUserVO).toString()))
                : error(DataConstants.UPDATE_FIELD);
    }


    @ApiOperation(value = "获取最新用户信息")
    @GetMapping("/getUser/{id}")
    @ResponseBody
    public  AjaxResult getUser(
            @ApiParam(value = "用户ID",name = "id",required = true)
            @PathVariable("id") Integer id) throws Exception {
        CmsUser cmsUserVO = cmsUserService.getDataById(id);
        if(!Optional.ofNullable(cmsUserVO).isPresent()){
            return error(DataConstants.NOT_USER);
        }
        return success(DataConstants.QUERY_DATA_SUCCESS, SecurityUtil.encrypt(JSONUtil.parse(cmsUserVO).toString()));
    }


    @ApiOperation(value = "会员说明")
    @ApiResponses({
            @ApiResponse(code = 0, message = DataConstants.CREATE_ORDER_SUCCESS),
            @ApiResponse(code = 500, message = DataConstants.CREATE_ORDER_FAIL),
    })
    @GetMapping("/buyMember/{mid}/{openid}")
    @ResponseBody
    public AjaxResult buyMember(
            @ApiParam(value = "所购买会员的ID",name = "mid",required = true)
            @PathVariable("mid")Integer mid,
            @ApiParam(value = "会员ID",name = "openid",required = true)
            @PathVariable("openid")String openid
            )
    {
//     查询所购买的会员等级是否存在
        Optional<CmsMemberType> memberType = Optional.ofNullable(cmsMemberService.getDataById(mid));
        if(!memberType.isPresent()){
            return error(DataConstants.CREATE_ORDER_FAIL);
        }
        Optional<CmsUser> user = Optional.ofNullable(cmsUserService.selectUserByOpenId(openid));
        if(!user.isPresent()){
            return error(DataConstants.NOT_USER);
        }
//     创建订单
        CmsOrder order = ReflectUtil.newInstance(CmsOrder.class);
        order.setMid(memberType.get().getId());
        order.setPrice(BigDecimal.valueOf(memberType.get().getPrice()));
        order.setPayType(1);
        order.setSinglePrice(BigDecimal.valueOf(memberType.get().getPrice()));
        order.setTotalNumber(1);
        order.setUid(user.get().getId());
        order.setOrderNo(SnowflakeIdWorker.getUUID());
        int res = orderService.insert(order);
        if(res <= 0){
            return error(DataConstants.CREATE_ORDER_FAIL);
        }
        return orderService.unifiedOrder(order.getOrderNo(),user.get().getOpenId());
    }


    @PostMapping({"/wechatnotify"})
    @ResponseBody
    public void wechatnotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug("微信回调");
        String notify = orderService.notify(request);
        logger.info(notify);
        PrintWriter writer = response.getWriter();
        writer.print(notify);
        writer.close();
    }


}
