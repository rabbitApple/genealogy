package com.ruoyi.web.controller.common;


/**
 * 常量
 * @author Penglei
 * @date 2020年5月27日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
public class RConstants {

    public static final String GET_SUCCESS = "获取成功";
    public static final String GET_ERROR = "获取失败";
}
