package com.ruoyi.web.controller.cms;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CmsInfo;
import com.ruoyi.system.domain.CmsMenu;
import com.ruoyi.system.domain.VO.CmsInfoVO;
import com.ruoyi.system.service.ICmsInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 信息控制器
 *
 * @author Penglei
 * @version 1.0.0
 * @date 2020年5月26日14:01:55
 * @updateVersion 1.0.0
 */
@Controller
@RequestMapping("/cms/info")
@Slf4j
public class InfoController extends BaseController {

    private String prefix = "cms/info/";

    @Autowired
    private ICmsInfoService cmsInfoService;


    //--------------------------------------------------视图转向区域-------------------------------------
    @RequiresPermissions("cms:info:view")
    @GetMapping()
    public String index() {
        return prefix + "index";
    }

    @RequiresPermissions("cms:info:add")
    @GetMapping("/add")
    public String add() {
        return prefix + "add";
    }

    /**
     * 查询公告列表
     */
    @RequiresPermissions("cms:info:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsInfo cmsInfo) {
        startPage();
        List<CmsInfoVO> list = cmsInfoService.selectList(cmsInfo);
        return getDataTable(list);
    }


    @RequiresPermissions("cms:info:edit")
    @GetMapping("/edit/{id}")
    public String add(@PathVariable("id") Integer id, ModelMap modelMap) {
        modelMap.put("info", cmsInfoService.getDataById(id));
        return prefix + "edit";
    }


    //----------------------------------------------------实际操作区

    /**
     * 新增保存菜单
     */
    @Log(title = "信息管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("cms:info:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated CmsInfo info) {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(cmsInfoService.checkNameUnique(info.getTitle(),-1))) {
            return error("发布资讯'" + info.getTitle() + "'失败，已存在相同的标题");
        }
        return toAjax(cmsInfoService.insert(info));
    }


    /**
     * 修改信息
     */
    @Log(title = "资讯管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("cms:info:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(CmsInfo info) {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(cmsInfoService.checkNameUnique(info.getTitle(),info.getId()))) {
            return error("更新资讯'" + info.getTitle() + "'失败，已存在相同的标题");
        }
        return toAjax(cmsInfoService.edit(info));
    }

    /**
     * 删除信息
     */
    @RequiresPermissions("cms:info:remove")
    @Log(title = "资讯", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(cmsInfoService.deleteByIds(ids));
    }

}
