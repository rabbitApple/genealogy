package com.ruoyi.web.controller.cms;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CmsMemberType;
import com.ruoyi.system.domain.CmsZupor;
import com.ruoyi.system.service.ICmsMemberService;
import com.ruoyi.system.service.ICmsZuporService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 会员类型
 * @author Penglei
 * @date  2020年6月5日
 * @version 1.0.0
 * @updateVersion 1.0.0
 */
@Controller
@RequestMapping("/cms/mtype")
@Slf4j
public class MemberController extends BaseController {

    private String prefix = "cms/mtype/";

    @Autowired
    private ICmsMemberService cmsMemberService;

//    ------------------------------------------------------------视图定向区-----------------------------

    @RequiresPermissions("cms:mtype:view")
    @GetMapping()
    public String index()
    {
        return prefix+"index";
    }


    @RequiresPermissions("cms:mtype:add")
    @GetMapping("/add")
    public String add()
    {
        return prefix+"add";
    }


    @RequiresPermissions("cms:mtype:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap modelMap)
    {
        modelMap.put("info",cmsMemberService.getDataById(id));
        return prefix+"edit";
    }





//   --------------------------------------------------------------数据操作区--------------------------


    /**
     * 族谱案例列表
     * @author Penglei
     * @date 2020年6月4日
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param cmsMemberType
     * @return  TableDataInfo
     */
    @RequiresPermissions("cms:mtype:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsMemberType cmsMemberType)
    {
        startPage();
        return getDataTable(cmsMemberService.dataList(cmsMemberType));
    }


    /**
    * 保存族谱案例
    * @author Penglei
    * @date 2020年6月4日
    * @version 1.0.0
    * @updateVersion 1.0.0
    * @param memberType
    * @return  AjaxResult
    */
    @RequiresPermissions("cms:mtype:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(CmsMemberType memberType)
    {
        return toAjax(cmsMemberService.insert(memberType));
    }

    /**
     * 修改族谱
     * @author Penglei
     * @date 2020年6月4日
     * @version 1.0.0
     * @updateVersion 1.0.0
     * @param memberType
     * @return  AjaxResult
     */
    @RequiresPermissions("cms:mtype:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(CmsMemberType memberType)
    {
        return toAjax(cmsMemberService.edit(memberType));
    }


    /**
     * 批量删除
     */
    @RequiresPermissions("cms:mtype:remove")
    @Log(title = "批量删除", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(cmsMemberService.deleteByIds(ids));
    }



}
