package com.ruoyi.web.controller.cms;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.CmsConfig;
import com.ruoyi.system.service.ICmsConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cms/config")
public class ConfigController extends BaseController {

    /**
     * 页面路径
     */
    String prefix = "cms/config";


    @Autowired
    private ICmsConfigService cmsConfigService;


    @Log(title = "内容管理参数配置")
    @RequiresPermissions("cms:config:view")
    @GetMapping()
    public String index(ModelMap modelMap)
    {
        modelMap.put("config",cmsConfigService.getDataById(1));
        return  prefix + "/index";
    }

    @Log(title = "修改cms参数")
    @RequiresPermissions("cms:config:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(CmsConfig cmsConfig)
    {
        return cmsConfigService.edit(cmsConfig) > 0
                ? success("修改成功")
                : error("修改失败");
    }


}
